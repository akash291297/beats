-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 26, 2020 at 07:23 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beats`
--

-- --------------------------------------------------------

--
-- Table structure for table `addbatch`
--

CREATE TABLE `addbatch` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class_id` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `center` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `starttime` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `endtime` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `addbatch`
--

INSERT INTO `addbatch` (`id`, `name`, `class_id`, `subject_id`, `center`, `date`, `starttime`, `endtime`, `time`, `created_at`, `updated_at`, `deleted_at`) VALUES
(8, 'sdfdf', 'Class', 'Maths', 'Molarband', '2019-12-31', '12:59', '12:59', NULL, '2019-10-14 01:51:56', '2020-02-04 22:47:36', '2020-02-04 22:47:36'),
(7, 'cx 11 2 sdfdf', 'Class', 'Maths', 'Molarband', '2018-12-30', '12:59', '12:55', NULL, '2019-10-14 01:16:56', '2020-02-04 22:47:31', '2020-02-04 22:47:31'),
(9, 'Bacth1', '1', '5', '2', '1582848000', '1580896800', '1580896800', '1580876421', '2020-02-04 19:41:31', '2020-02-04 22:50:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `adminprofiles`
--

CREATE TABLE `adminprofiles` (
  `id` int(10) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `qualification` varchar(50) DEFAULT NULL,
  `time` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `adminprofiles`
--

INSERT INTO `adminprofiles` (`id`, `name`, `email`, `image`, `qualification`, `time`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Hari', 'akashsingh000@yahoo.in', 'storage/adminProfile/2020/02/16/6bo9vmbA41fi871oDwkcO6zLwPIk4VcUI9ZG02c9.jpeg', 'Btdgf', NULL, '2020-02-14 05:36:42', '2020-02-16 06:49:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `time` varchar(50) DEFAULT NULL,
  `created_at` varchar(50) DEFAULT NULL,
  `updated_at` varchar(50) DEFAULT NULL,
  `deleted_at` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `time`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'HARI SHANKAR SINGH', 'akashsingh994@gmail.com', '$2y$10$w2kpW8wQVjxotdruBEgUZ.iDzc/IImGN5kfwx2Pl/P0z8eMTTG34W', '1580018392', '2020-01-26 05:59:52', '2020-02-17 03:29:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `announcement` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `batch` int(10) DEFAULT NULL,
  `time` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `announcements`
--

INSERT INTO `announcements` (`id`, `announcement`, `image`, `batch`, `time`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Hy23', 'storage/announcements/2020/01/30/1qLgEC6cmy680AEvtxJJfct9B6ubDFiBuj2L0T2e.jpeg', NULL, '1580358705', '2020-01-29 22:45:38', '2020-01-29 23:01:45', NULL),
(3, 'Hy23', 'storage/announcements/2020/01/30/Du7x90uytPOWHL7hnMf9sAKwkBddKUlEQmW12que.jpeg', NULL, '1580358689', '2020-01-29 22:47:48', '2020-01-29 23:01:33', '2020-01-29 23:01:33'),
(6, 'Ram', 'storage/announcements/2020/02/25/FLsgTJK2kBs1d5oN0cKd8gsO2fTf6KqPterNJl03.jpeg', 9, '1582615422', '2020-02-25 01:53:42', '2020-02-25 01:53:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `announcement_roll`
--

CREATE TABLE `announcement_roll` (
  `id` int(10) NOT NULL,
  `announce_id` int(10) DEFAULT NULL,
  `roll_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `announcement_roll`
--

INSERT INTO `announcement_roll` (`id`, `announce_id`, `roll_id`) VALUES
(1, 5, 5),
(2, 6, 5);

-- --------------------------------------------------------

--
-- Table structure for table `assignments`
--

CREATE TABLE `assignments` (
  `id` int(10) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `topic` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subdate` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assignments`
--

INSERT INTO `assignments` (`id`, `name`, `topic`, `subdate`, `attachment`, `time`, `created_at`, `updated_at`, `deleted_at`) VALUES
(8, 'mkm', 'kmlk', 'mklmk', 'Bank Passbook.jpg', NULL, NULL, '2020-01-30 00:13:40', '2020-01-30 00:13:40'),
(2, 'new', 'adf', '30/01/2020 12:00 AM - 30/01/2020 11:59 PM', 'storage/announcements/2020/01/30/LIuZcrLOx7ZqEpYeDn0hyhxChSm5c5PPni6gKh25.jpeg', '1580362474', '2020-01-30 00:04:34', '2020-01-30 00:27:49', '2020-01-30 00:27:49'),
(3, 'gr', 'we', '30/01/2020 12:00 AM - 30/01/2020 11:59 PM', 'storage/announcements/2020/01/30/Tr6XyJq77oyRJjI5xgVILFTuaOJYeI3UnZmWHXSs.jpeg', '1580362820', '2020-01-30 00:10:20', '2020-01-30 00:28:16', '2020-01-30 00:28:16'),
(1, 'asdf123455', 'asdfweewr', '30/01/2020 12:00 AM - 31/01/2020 12:00 AM', 'storage/assignments/2020/01/30/TO1bEvzRQGvcbeD0FNz3NbBXdTOhErlWjyfArhEe.png', '1580363912', '2020-01-30 00:12:06', '2020-01-30 00:28:32', NULL),
(5, 'gr23', 'we12', '30/01/2020 06:00 AM - 31/01/2020 02:00 AM', NULL, '1580363627', '2020-01-30 00:23:47', '2020-01-30 00:28:12', '2020-01-30 00:28:12'),
(6, 'new', 'adf', '30/01/2020 12:00 AM - 31/01/2020 12:00 AM', 'storage/assignments/2020/01/30/4Cl12DACaJfcAsYRDZKVxYJQb8qQeSlXbv1tsr5z.jpeg', '1580363885', '2020-01-30 00:24:03', '2020-01-30 00:28:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `batch_week`
--

CREATE TABLE `batch_week` (
  `id` int(10) NOT NULL,
  `batch_id` int(10) DEFAULT NULL,
  `week_id` int(10) DEFAULT NULL,
  `time` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `batch_week`
--

INSERT INTO `batch_week` (`id`, `batch_id`, `week_id`, `time`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 9, 2, NULL, '2020-02-04 19:41:31', '2020-02-04 22:45:47', '2020-02-04 22:45:47'),
(2, 9, 5, NULL, '2020-02-04 19:41:31', '2020-02-04 22:45:47', '2020-02-04 22:45:47'),
(3, 9, 5, '1580876147', '2020-02-04 22:45:47', '2020-02-04 22:47:56', '2020-02-04 22:47:56'),
(4, 9, 6, '1580876147', '2020-02-04 22:45:47', '2020-02-04 22:47:56', '2020-02-04 22:47:56'),
(5, 9, 1, '1580876276', '2020-02-04 22:47:56', '2020-02-04 22:50:21', '2020-02-04 22:50:21'),
(6, 9, 5, '1580876421', '2020-02-04 22:50:21', '2020-02-04 22:50:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `facultys`
--

CREATE TABLE `facultys` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `facultys`
--

INSERT INTO `facultys` (`id`, `name`, `subject`, `class`, `time`, `created_at`, `updated_at`, `deleted_at`) VALUES
(16, 'HARI SHANKAR SINGH', 'maths123', '12', '1580376724', '2020-01-30 04:01:46', '2020-01-30 04:02:09', '2020-01-30 09:32:09'),
(17, 'HARI SHANKAR SINGH', 'maths', '12', '1580376744', '2020-01-30 04:02:24', '2020-01-30 04:02:24', NULL),
(18, 'akash', 'maths', '12', '1580376762', '2020-01-30 04:02:42', '2020-01-30 04:02:42', NULL),
(19, 'akash', 'maths', '12', '1580376770', '2020-01-30 04:02:50', '2020-01-30 04:03:11', '2020-01-30 09:33:11');

-- --------------------------------------------------------

--
-- Table structure for table `staticpage`
--

CREATE TABLE `staticpage` (
  `id` int(11) NOT NULL,
  `banner` varchar(100) DEFAULT NULL,
  `banner1` varchar(100) DEFAULT NULL,
  `banner2` varchar(100) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `courses` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `time` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `staticpage`
--

INSERT INTO `staticpage` (`id`, `banner`, `banner1`, `banner2`, `address`, `courses`, `created_at`, `updated_at`, `deleted_at`, `time`) VALUES
(1, 'storage/banner/2020/02/26/5upRQu5yUZkciD2VlutRyExFsZZMQp0ML8HsHXzO.jpeg', NULL, NULL, '<p>Address: Beats Academy Molarband, Badarpur, New Delhi</p>\r\n\r\n<p>Phone:&nbsp;<a href=\"http://127.0.0.1:8000/#!\">+101-1231-4321</a></p>\r\n\r\n<p>Email:&nbsp;<a href=\"http://127.0.0.1:8000/#!\">info@educationmaster.com</a></p>', 'Physical Education23', '2020-02-25 23:36:35', '2020-02-26 00:01:57', '2020-02-26 00:01:57', '1582695050'),
(2, 'storage/banner/2020/02/26/8nxYkmwEuuL80ZAi5uMBccK8XO2lHon38JIgVshK.jpeg', NULL, NULL, '<p>Address: Beats Academy Molarband, Badarpur, New Delhi</p>\r\n\r\n<p>Phone:&nbsp;<a href=\"http://127.0.0.1:8000/#!\">+101-1231-4321</a></p>\r\n\r\n<p>Email:&nbsp;<a href=\"http://127.0.0.1:8000/#!\">info@educationmaster.com</a></p>', 'Maths English', '2020-02-26 00:02:16', '2020-02-26 00:04:33', '2020-02-26 00:04:33', '1582695177'),
(3, 'storage/banner/2020/02/26/HblqUHQ4jdITWKMGUU2NyJcXeLIhVuxRDtlUWB2n.jpeg', NULL, NULL, '<p>Address: Beats Academy, Molarband,Badarpur,New Delhi</p>\r\n\r\n<p>Phone:&nbsp;<a href=\"http://127.0.0.1:8000/#!\">+101-1231-4321</a></p>\r\n\r\n<p>Email:&nbsp;<a href=\"http://127.0.0.1:8000/#!\">info@educationmaster.com</a></p>', 'mathsenglishhindiscience', '2020-02-26 00:06:23', '2020-02-26 00:38:37', '2020-02-26 00:38:37', '1582695512'),
(4, 'storage/banner/2020/02/26/q36oKn1kNnuFGCodNcGKntmjBbJe5LVKIQYQyYsd.jpeg', 'storage/banner/2020/02/26/RE2PCQQSZYCebPrnQb52XbKyrsG5UmbXF7W4Modt.jpeg', 'storage/banner/2020/02/26/R5bJ31rDbtLDelc8oapSnMf1vF8zOTtIQLsBwnYH.jpeg', '<p>Address: Beats Academy Molarband, Badarpur, New Delhi</p>\r\n\r\n<p>Phone:&nbsp;<a href=\"http://127.0.0.1:8000/#!\">+101-1231-4321</a></p>\r\n\r\n<p>Email:&nbsp;<a href=\"http://127.0.0.1:8000/#!\">info@educationmaster.com</a></p>', 'maths', '2020-02-26 00:27:30', '2020-02-26 00:48:17', NULL, '1582697897');

-- --------------------------------------------------------

--
-- Table structure for table `studentadmins`
--

CREATE TABLE `studentadmins` (
  `id` int(11) NOT NULL,
  `roll_no` int(10) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `time` varchar(50) DEFAULT NULL,
  `created_at` varchar(50) DEFAULT NULL,
  `updated_at` varchar(50) DEFAULT NULL,
  `deleted_at` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `studentadmins`
--

INSERT INTO `studentadmins` (`id`, `roll_no`, `name`, `email`, `password`, `time`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, 'akash', 'akflorist74@gmail.com', '$2y$10$6KiB23NmJzrqoQBwzS//UOWywS75Bijc3ELqvr39lkHY7YVgDVXwa', '1580613455', '2020-02-02 03:17:35', '2020-02-02 03:17:35', NULL),
(2, 1, 'HARI SHANKAR SINGH', 'akashsingh000@yahoo.in', '$2y$10$dUA60.OCTBjZD1FbaqbQ2OjlH3POEulkuA4xB4MtNRmDiaAYefUge', '1581918389', '2020-02-17 05:46:29', '2020-02-17 05:46:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `studentclass`
--

CREATE TABLE `studentclass` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `class` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'a=active n=inactive',
  `time` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `studentclass`
--

INSERT INTO `studentclass` (`id`, `class`, `status`, `time`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '9', 'n', NULL, '2019-10-05 10:21:38', '2020-02-22 00:25:40', NULL),
(2, '10', 'a', '1580544502', '2019-10-06 23:09:11', '2020-02-01 04:51:17', NULL),
(3, '21', NULL, '1580544489', '2020-02-01 02:37:01', '2020-02-01 02:38:15', '2020-02-01 02:38:15'),
(4, '11', 'a', '1580545580', '2020-02-01 02:56:20', '2020-02-01 04:45:59', NULL),
(5, '12', 'n', '1580545966', '2020-02-01 03:02:46', '2020-02-04 01:04:56', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(10) NOT NULL,
  `roll_no` int(10) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profilepic` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `batch` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mother` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fcontact` bigint(15) DEFAULT NULL,
  `mcontact` bigint(15) DEFAULT NULL,
  `scontact` bigint(15) DEFAULT NULL,
  `time` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `roll_no`, `name`, `class`, `school`, `profilepic`, `batch`, `address`, `father`, `mother`, `fcontact`, `mcontact`, `scontact`, `time`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, 'akash', '12', 'sfddfdsdssd', 'storage/images/2020/01/29/Udjqj0YR7tZtRcsLfqD4JLnDc5M2faq8205TjIsv.jpeg', '9', 'wwe', 'first', 'scdscscscsc', 9874563210, 9990326547, 9874563210, '1580357306', '2020-01-29 02:41:43', '2020-02-03 21:45:46', '2020-02-03 21:45:46'),
(2, NULL, 'HARI SHANKAR SINGH', '12', 'sfddfdsdssd', 'storage/images/2020/01/29/jadymkOgZjaC52mNlNF9UuWiTJpU7UX0hOk9l9g9.jpeg', '9', 'cxvvc', 'first123', 'scdscscscsc', 9874563210, 9990326547, 9874563210, '1580352208', '2020-01-29 02:45:29', '2020-01-29 21:55:03', '2020-01-29 21:55:03'),
(3, NULL, 'HARI SHANKAR SINGH', '12', 'sdscd', 'storage/students/2020/02/02/ICglHiIXtpUwtUL2Ted9SHdOCtXEBAyehelAIJ3K.jpeg', '9', 'ed', 'first', 'scdscscscsc', 9874563210, 9990362694, 9874563210, '1580618820', '2020-02-01 23:17:00', '2020-02-21 23:42:46', '2020-02-21 23:42:46'),
(4, 1, 'HARI SHANKAR SINGH', '8', 'DAV', 'storage/students/2020/02/17/eejsCzCKhrN0MSxvfAdqR42OPtGR06rhuvfHXetU.jpeg', '9', 'houseno 32 street 26 f molarband extn badrpur new delhi', 'first', 'dsfafafa', 9874563210, 9990326547, 9874563210, '1581917584', '2020-02-17 00:03:04', '2020-02-21 23:42:50', '2020-02-21 23:42:50'),
(5, 1, 'HARI SHANKAR SINGH', '12', 'DAV', 'storage/students/2020/02/22/o6JKtKGN1arE5H8HupgYfkWLPq0x1qlk5sjrEEk4.jpeg', '9', 'houseno 32 street 26 f molarband extn badrpur new delhi123', 'sadasdsads', 'ascascsacsac', 9874563210, 9990326547, 9874563210, '1582348404', '2020-02-21 23:43:24', '2020-02-21 23:43:24', NULL),
(6, 2, 'HARI SHANKAR SINGH', '4', 'DAV', 'storage/images/2020/02/25/kHH9N0OXE0IoKAAbCXytNetRfX6aTyElYSUEEp0K.jpeg', '9', 'houseno 32 street 26 f molarband extn badrpur new delhi123', 'first', 'scdscscscsc', 9874563212, 9990326546, 9874563217, '1582623313', '2020-02-25 03:47:13', '2020-02-25 04:05:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(10) NOT NULL,
  `class_id` int(10) DEFAULT NULL,
  `subject` varchar(50) DEFAULT NULL,
  `time` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `class_id`, `subject`, `time`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'maths', '1580796626', '2020-02-04 00:03:46', '2020-02-04 00:40:32', '2020-02-04 00:40:32'),
(2, 1, 'Maths', '1580796924', '2020-02-04 00:45:24', '2020-02-04 00:45:24', NULL),
(3, 1, 'English', '1580797007', '2020-02-04 00:46:47', '2020-02-04 00:46:47', NULL),
(4, 1, 'Hindi', '1580797083', '2020-02-04 00:48:03', '2020-02-04 00:48:03', NULL),
(5, 1, 'Science', '1580797092', '2020-02-04 00:48:12', '2020-02-04 00:48:12', NULL),
(6, 2, 'Maths', '1580797126', '2020-02-04 00:48:46', '2020-02-04 00:48:46', NULL),
(7, 2, 'English', '1580797144', '2020-02-04 00:49:04', '2020-02-04 00:49:04', NULL),
(8, 2, 'Hindi', '1580797160', '2020-02-04 00:49:20', '2020-02-04 00:49:20', NULL),
(9, 2, 'Science', '1580797320', '2020-02-04 00:51:40', '2020-02-04 00:52:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `weekdays`
--

CREATE TABLE `weekdays` (
  `id` int(10) NOT NULL,
  `weekdays` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `weekdays`
--

INSERT INTO `weekdays` (`id`, `weekdays`) VALUES
(1, 'Sunday'),
(2, 'Monday'),
(3, 'Tuesday'),
(4, 'Wednesday'),
(5, 'Thrusday'),
(6, 'Friday'),
(7, 'Saturday');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addbatch`
--
ALTER TABLE `addbatch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `adminprofiles`
--
ALTER TABLE `adminprofiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announcement_roll`
--
ALTER TABLE `announcement_roll`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assignments`
--
ALTER TABLE `assignments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `batch_week`
--
ALTER TABLE `batch_week`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facultys`
--
ALTER TABLE `facultys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staticpage`
--
ALTER TABLE `staticpage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `studentadmins`
--
ALTER TABLE `studentadmins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `studentclass`
--
ALTER TABLE `studentclass`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `weekdays`
--
ALTER TABLE `weekdays`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addbatch`
--
ALTER TABLE `addbatch`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `adminprofiles`
--
ALTER TABLE `adminprofiles`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `announcement_roll`
--
ALTER TABLE `announcement_roll`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `assignments`
--
ALTER TABLE `assignments`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `batch_week`
--
ALTER TABLE `batch_week`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `facultys`
--
ALTER TABLE `facultys`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `staticpage`
--
ALTER TABLE `staticpage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `studentadmins`
--
ALTER TABLE `studentadmins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `studentclass`
--
ALTER TABLE `studentclass`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `weekdays`
--
ALTER TABLE `weekdays`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
