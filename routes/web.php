<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*FRONTEND ROUTES*/

Route::get('/','StaticPage\StaticPageController@slider')->name('home');
Route::get('/about','StaticPage\StaticPageController@about')->name('page.about');
Route::get('/admission','StaticPage\StaticPageController@admission')->name('page.admission');



/*ADMIN LOGIN FUNCTIONALITY*/


/*Admin Login Page View*/
Route::get('/admin','Admin\ViewController@loginPage')->name('admin.login');
/*Admin Register Page View*/
Route::get('/admin/register','Admin\ViewController@registerPage')->name('admin.register');
/*Registration Page*/
Route::post('admin-register','Admin\RegisterController@register')->name('user.register');
/*Login Page*/
Route::post('admin-login','Admin\AdminController@login')->name('user.login');
/*Forgot Password*/
Route::get('admin-forgot','Admin\ForgotController@forgotPage')->name('admin.forgot');





/*Dashboard*/
Route::group(['middleware' => ['auth:admin']], function () {
    Route::get('dashboard','Admin\DashboardController@dashboard')->name('dashboard');
    Route::get('logout','Admin\DashboardController@logout')->name('logout');
    Route::post('password','Admin\DashboardController@password')->name('password');
    /*INSIDE ADMIN PANEL CONTENT*/
    Route::get('time-table','Admin\Content\TimeTableController@index')->name('timetable');

    /*STUDENT DATAS*/
    Route::get('student-index','Admin\Content\StudentController@index')->name('student.list');
    Route::get('student-create','Admin\Content\StudentController@create')->name('student.create');
    Route::post('student-store','Admin\Content\StudentController@store')->name('student.store');
	Route::get('student-edit/{id}','Admin\Content\StudentController@edit')->name('student.edit');
    Route::get('student-delete/{id}','Admin\Content\StudentController@destroy')->name('student.delete');
    Route::post('student-update','Admin\Content\StudentController@update')->name('student.update');

    /*ANNOUNCEMENTS DATAS*/
     Route::get('announcement-index','Admin\Content\AnnouncementController@index')->name('announcement.list');
    Route::get('announcement-create','Admin\Content\AnnouncementController@create')->name('announcement.create');
    Route::post('announcement-store','Admin\Content\AnnouncementController@store')->name('announcement.store');
    Route::get('announcement-edit/{id}','Admin\Content\AnnouncementController@edit')->name('announcement.edit');
    Route::get('announcement-delete/{id}','Admin\Content\AnnouncementController@destroy')->name('announcement.delete');
    Route::post('announcement-update','Admin\Content\AnnouncementController@update')->name('announcement.update');

    /*ASSIGNMENT DATAS*/
   Route::get('assignment-index','Admin\Content\AssignmentController@index')->name('assignment.list');
    Route::get('assignment-create','Admin\Content\AssignmentController@create')->name('assignment.create');
    Route::post('assignment-store','Admin\Content\AssignmentController@store')->name('assignment.store');
    Route::get('assignment-edit/{id}','Admin\Content\AssignmentController@edit')->name('assignment.edit');
    Route::get('assignment-delete/{id}','Admin\Content\AssignmentController@destroy')->name('assignment.delete');
    Route::post('assignment-update','Admin\Content\AssignmentController@update')->name('assignment.update');

    /*FACULTY DATAS*/
    Route::get('faculty-index','Admin\Content\FacultyController@index')->name('faculty.list');
    Route::get('faculty-create','Admin\Content\FacultyController@create')->name('faculty.create');
    Route::post('faculty-store','Admin\Content\FacultyController@store')->name('faculty.store');
    Route::get('faculty-edit/{id}','Admin\Content\FacultyController@edit')->name('faculty.edit');
    Route::get('faculty-delete/{id}','Admin\Content\FacultyController@destroy')->name('faculty.delete');
    Route::post('faculty-update','Admin\Content\FacultyController@update')->name('faculty.update');

    /*STUDENT CLASS*/
    Route::get('class-index','Admin\Content\ClassController@index')->name('class.list');
    Route::get('class-create','Admin\Content\ClassController@create')->name('class.create');
    Route::post('class-store','Admin\Content\ClassController@store')->name('class.store');
    Route::get('class-edit/{id}','Admin\Content\ClassController@edit')->name('class.edit');
    Route::get('class-delete/{id}','Admin\Content\ClassController@destroy')->name('class.delete');
    Route::post('class-update','Admin\Content\ClassController@update')->name('class.update');
    Route::post('class-active','Admin\Content\ClassController@active')->name('class.active');
    Route::post('class-inactive','Admin\Content\ClassController@inactive')->name('class.inactive');

    /*SUBJECTS DATAS*/  
    Route::get('subject-index','Admin\Content\SubjectController@index')->name('subject.list');
    Route::get('subject-create','Admin\Content\SubjectController@create')->name('subject.create');
    Route::post('subject-store','Admin\Content\SubjectController@store')->name('subject.store');
    Route::get('subject-edit/{id}','Admin\Content\SubjectController@edit')->name('subject.edit');
    Route::get('subject-delete/{id}','Admin\Content\SubjectController@destroy')->name('subject.delete');
    Route::post('subject-update','Admin\Content\SubjectController@update')->name('subject.update');

    /*BATCHES DATAS*/
    Route::get('batch-index','Admin\Content\BatchController@index')->name('batch.list');
    Route::get('batch-create','Admin\Content\BatchController@create')->name('batch.create');
    Route::post('batch-store','Admin\Content\BatchController@store')->name('batch.store');
    Route::get('batch-edit/{id}','Admin\Content\BatchController@edit')->name('batch.edit');
    Route::get('batch-delete/{id}','Admin\Content\BatchController@destroy')->name('batch.delete');
    Route::post('batch-update','Admin\Content\BatchController@update')->name('batch.update');

    /*AJAX*/
    Route::get('get-subject','Admin\Content\BatchController@getSubject');
    Route::get('get-roll','Admin\Content\AnnouncementController@getRoll'); 

    /*ADMIN PROFILE UPDATE*/
    Route::get('admin-profile','Admin\Content\ProfileController@index')->name('admin.profile');
    Route::post('admin-store','Admin\Content\ProfileController@store')->name('admin.store');
    Route::get('admin-edit/{id}','Admin\Content\ProfileController@edit')->name('admin.edit');
    Route::post('admin-update','Admin\Content\ProfileController@update')->name('admin.update');
    

    /*STATIC PAGES DATAS*/  
    Route::get('static-index','Admin\Content\StaticPageController@index')->name('static.list');
    Route::get('static-create','Admin\Content\StaticPageController@create')->name('static.create');
    Route::post('static-store','Admin\Content\StaticPageController@store')->name('static.store');
    Route::get('static-edit/{id}','Admin\Content\StaticPageController@edit')->name('static.edit');
    Route::get('static-delete/{id}','Admin\Content\StaticPageController@destroy')->name('static.delete');
    Route::post('static-update','Admin\Content\StaticPageController@update')->name('static.update');

});
	


/*STUDENT LOGIN FUNCTIONALITY*/

/*Student Login Page View*/
Route::get('/student','Student\ViewController@loginPage')->name('student.login');
/*Student Register Page View*/
Route::get('/student/register','Student\ViewController@registerPage')->name('student.register');
/*Registration Page*/
Route::post('student-register','Student\RegisterController@register')->name('register.student');
/*Login Page*/
Route::post('student-login','Student\StudentController@login')->name('login.student');
/*Dashboard*/
Route::group(['middleware' => ['auth:student']], function () {
    Route::get('student-dashboard','Student\DashboardController@dashboard')->name('student.dashboard');
    Route::get('student-logout','Student\DashboardController@logout')->name('student.logout');

    /*INSIDE ADMIN PANEL*/
     Route::get('my-details','Student\content\MyDetailController@index')->name('details');
     Route::get('my-announcements','Student\content\MyDetailController@announcement')->name('announcement');
     Route::get('my-assignment','Student\content\MyDetailController@assignment')->name('assignment');

});


