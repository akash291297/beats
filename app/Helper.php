<?php
	
	use App\AdminModel\AdminProfile;
	use App\AdminModel\Student;
	use App\AdminModel\Batch;
	use App\AdminModel\StudentClass;
	use App\AdminModel\StaticPage;

	function profile(){
		$profile = AdminProfile::orderBy('id','desc')->get();
		return $profile;
	}

	function students(){
		$student = Student::orderBy('id','desc')->get();
		return $student;
	}

	function batchs(){
		$batch = Batch::orderBy('id','desc')->get();
		return $batch;
	}

	function classes(){
		$class = StudentClass::orderBy('id','desc')->get();
		return $class;
	}

	function staticpage(){
		$static = StaticPage::orderBy('id','desc')->get();
		return $static;
	}

?>