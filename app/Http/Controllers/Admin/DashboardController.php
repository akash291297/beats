<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator,Auth;
use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth:admin');
    }

    public function dashboard()     
    {
        return view('layouts.main');
    }

    public function password(Request $request){

        Validator::make($request->all(),[
            "oldpassword"      => "required",
            "newpassword"      => "required|required_with:oldpassword",
            "confirmpassword"  => "same:newpassword"
        ],[
            "oldpassword.required"      => "Old password is required",
            "newpassword.required_with" => "New password required with old password"
        ])->validate();

        $user = Auth::user();
        if($request->oldpassword){
            if(!(Hash::check($request->get('oldpassword'),Auth::user()->password))){
                return redirect()->route('admin.profile')->with('error','Your old password does not matches with the password you provided. Please try again.');
            }

            if(strcmp($request->get('oldpassword'),$request->get('newpassword'))==0){
                return redirect()->route('admin.profile')->with("error","New Password cannot be same as your old password. Please choose a different");
            }

            $user->password = bcrypt($request->get('newpassword'));
        }

        $user->save();
        return redirect()->route('admin.profile')->with('success',"Password changed successfully");

    }
    public function logout(Request $request){
        
    	Auth::guard('admin')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        $notification = array(
            'message'    => 'You Are Logged Out Successfully!!', 
            'alert-type' => 'success'
        );
        return redirect()->route('admin.login')->with($notification);   

    }
}
