<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Admin;
use Validator;


class RegisterController extends Controller
{
    public function register(Request $request){
    	Validator::make($request->all(),[
    		"name"     =>  "required",
    		"email"    =>  "required|email|unique:admins,email",
    		"password" =>  "required",
    		"retype"   =>  "required|same:password"
     	])->validate();

     	$adminRegister           =   new Admin();
     	$adminRegister->name     =   $request->name;
     	$adminRegister->email    =   $request->email;
     	$adminRegister->password =   bcrypt($request->password);
     	$adminRegister->time     =   time();
     	$adminRegister->save();
     	$notification = array(
			'message' => 'You Are Logged In Successfully!!', 
			'alert-type' => 'success'
		);
     	return redirect()->route('dashboard')->with($notification);

    }
}
