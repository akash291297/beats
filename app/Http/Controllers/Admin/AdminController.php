<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Admin;
use Validator,Auth;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }
    
    public function login(Request $request){
        
        Validator::make($request->all(),[
            "email"    =>  "required|email",
            "password" =>  "required|max:10"
        ])->validate();


        if(Auth::guard('admin')->attempt($request->only('email','password'))){
            return redirect()->route('dashboard')->with('success','Login Successfully');
        }
    }

   
    protected function guard()
    {
        return Auth::guard('admin');
    }
}
