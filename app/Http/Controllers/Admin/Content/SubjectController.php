<?php

namespace App\Http\Controllers\Admin\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AdminModel\Subject;
use App\AdminModel\StudentClass;
use Validator;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjectLists =  Subject::with('class')->orderBy('id','asc')->get();
        return view('admin.content.subjects.index',compact('subjectLists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classLists = StudentClass::orderBy('id','asc')->get();
        return view('admin.content.subjects.create',compact('classLists'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(),[
            "class_id"    =>  "required",
            "subject"     =>  "required",

        ])->validate();

        $subjectData            =  new Subject;
        $subjectData->class_id  =  $request->class_id;
        $subjectData->subject   =  $request->subject;
        $subjectData->time      =   time();
        $subjectData->save();
        return redirect()->route('subject.list')->with('success','Subject Created Successfully');

       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classLists   =   StudentClass::orderBy('id','asc')->get();
        $subjectEdit  =   Subject::find($id);
        return view('admin.content.subjects.edit',compact('classLists'))->with('subjectEdit',$subjectEdit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        Validator::make($request->all(),[
            "subject_id"  =>  "required|numeric",
            "class_id"    =>  "required",
            "subject"     =>  "required",

        ])->validate();

        $subjectData            =  Subject::find($request->subject_id);
        $subjectData->class_id  =  $request->class_id;
        $subjectData->subject   =  $request->subject;
        $subjectData->time      =   time();
        $subjectData->save();
        return redirect()->route('subject.list')->with('success','Subject Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteSubject = Subject::find($id);
        $deleteSubject -> delete();
        return redirect()->route('subject.list')->with('success','Subject Deleted Successfully');  
    }
}
