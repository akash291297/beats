<?php

namespace App\Http\Controllers\Admin\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AdminModel\StudentClass;
use Validator;

class ClassController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classDetails = StudentClass::orderby('id','desc')->get();
        return view('admin.content.studentclass.index',compact("classDetails"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.content.studentclass.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Validator::make($request->all(),[
            "class"  => "required",
            "status" => "required"
        ],
        [
            "class.required" => "Enter Your Class"    
        ]
    )->validate();

        $classData          =  new StudentClass;
        $classData->class   =  $request->class;
        $classData->status  =  $request->status;
        $classData->time    =  time();
        $classData->save();
        return redirect()->route('class.list')->with('success','Class Created Successfully');


    }

    public function active(Request $request){
      
        $classData = StudentClass::find($request->aclass_id);
        $classData->status = 'a';
        $classData->save();
        return redirect()->route('class.list')->with('success','Class Status Active');
    }

    public function inactive(Request $request)
    {
        $classData = StudentClass::find($request->nclass_id);
        $classData->status = 'n';
        $classData->save();
        return redirect()->route('class.list')->with('success','Class Status Inactive');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classEdit = StudentClass::find($id);
        return view('admin.content.studentclass.edit')->with('classEdit',$classEdit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        Validator::make($request->all(),[
            "sclass_id" => "required|numeric",
            "class"     => "required"
        ],
        [
            "class.required" => "Enter Your Class"    
        ]
    )->validate();

        $classData         =  StudentClass::find($request->sclass_id);
        $classData->class  =  $request->class;
        $classData->time   =  time();
        $classData->save();
        return redirect()->route('class.list')->with('success','Class Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $classDelete = StudentClass::find($id);
        $classDelete -> delete();
        return redirect()->route('class.list')->with('success','Class Deleted Successfully');
    }
}
