<?php

namespace App\Http\Controllers\Admin\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AdminModel\Assignment;
use Validator;

class AssignmentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assignmentDetails = Assignment::orderby('id','desc')->get();
        return view('admin.content.assignments.index',compact("assignmentDetails"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.content.assignments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $announcementsData = Validator::make($request->all(),[
            "text" => "required",
            "topic" => "required",
            "date"  => "required",
        ],[

            "text.required" => "Enter Your Assignment"
        ])->validate();

        $assignmentsData          =  new Assignment;
        $assignmentsData->name    =  $request->text;
        $assignmentsData->topic   =  $request->topic;
        $assignmentsData->subdate =  $request->date;

        if($request->hasFile('image')){
            $assignmentsData->attachment  =  'storage/'.$request->file('image')->store('assignments/'.date('Y/m/d'));
        }
        $assignmentsData->time     =   time();
        $assignmentsData->save();
        return redirect()->route('assignment.list')->with('success','Assignment Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $assignmentEdit = Assignment::find($id);
        return view('admin.content.assignments.edit')->with('assignmentEdit',$assignmentEdit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       $assignmentData = Validator::make($request->all(),[
            "assignment_id" =>  "required|numeric",
            "text"          =>  "required",
            "topic"         =>  "required",
            "date"          =>  "required",
        ],[

            "text.required" => "Enter Your Assignment"
        ])->validate();

        $assignmentsData          =  Assignment::find($request->assignment_id);
        $assignmentsData->name    =  $request->text;
        $assignmentsData->topic   =  $request->topic;
        $assignmentsData->subdate =  $request->date;

        if($request->hasFile('image')){
            $assignmentsData->attachment  =  'storage/'.$request->file('image')->store('assignments/'.date('Y/m/d'));
        }
        $assignmentsData->time     =   time();
        $assignmentsData->save();
        return redirect()->route('assignment.list')->with('success','Assignment Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $assignmentDelete = Assignment::find($id);
        $assignmentDelete -> delete();
        return redirect()->route('assignment.list')->with('success','Assignment Deleted Successfully');
    }
}
