<?php

namespace App\Http\Controllers\Admin\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\AdminModel\StaticPage;

class StaticPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staticList = StaticPage::orderBy('id','desc')->get();
        return view('admin/content/staticpage/index',compact('staticList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/content/staticpage/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(),[
            "editor1" => "required|nullable",
            "courses" => "required|nullable",
        ],[ 
            "editor1.required" => "Enter Your Address",
            "courses.required" => "Enter Your Courses"
        ])->validate();

        $staticData = new StaticPage();
        if($request->hasFile('image')){
          $staticData->banner =  'storage/'.$request->file('image')->store('banner/'.date('Y/m/d'));
        }
        if($request->hasFile('image1')){
          $staticData->banner1 =  'storage/'.$request->file('image1')->store('banner/'.date('Y/m/d'));
        }
        if($request->hasFile('image2')){
          $staticData->banner2 =  'storage/'.$request->file('image2')->store('banner/'.date('Y/m/d'));
        }
        $staticData->address = $request->editor1;
        $staticData->courses = $request->courses;
        $staticData->time    = time();
        $staticData->save();
        return redirect()->route('static.list')->with('success','StaticPage Created Successfully');
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $staticEdit = StaticPage::find($id);
        return view('admin/content/staticpage/edit')->with('staticEdit',$staticEdit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       Validator::make($request->all(),[
            "static_id" => "required",
            "editor1"   => "required|nullable",
            "courses"   => "required|nullable",
        ],[ 
            "editor1.required" => "Enter Your Address",
            "courses.required" => "Enter Your Courses"
        ])->validate();

        $staticData = StaticPage::find($request->static_id);
        if($request->hasFile('image')){
          $staticData->banner =  'storage/'.$request->file('image')->store('banner/'.date('Y/m/d'));
        }
         if($request->hasFile('image1')){
          $staticData->banner1 =  'storage/'.$request->file('image1')->store('banner/'.date('Y/m/d'));
        }
        if($request->hasFile('image2')){
          $staticData->banner2 =  'storage/'.$request->file('image2')->store('banner/'.date('Y/m/d'));
        }
        $staticData->address = $request->editor1;
        $staticData->courses = $request->courses;
        $staticData->time    = time();
        $staticData->save();
        return redirect()->route('static.list')->with('success','StaticPage Created Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $staticDelete = StaticPage::find($id);
        $staticDelete->delete();
        return redirect()->route('static.list')->with('success','StaticPage Deleted Successfully');
    }
}
