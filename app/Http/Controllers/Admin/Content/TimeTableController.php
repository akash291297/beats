<?php

namespace App\Http\Controllers\Admin\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TimeTableController extends Controller
{

	public function __construct()
    {
    	$this->middleware('auth:admin');
    }

    public function index(){
    	return view('admin.content.timetable');
    }
}
