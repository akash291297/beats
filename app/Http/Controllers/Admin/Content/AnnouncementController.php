<?php

namespace App\Http\Controllers\Admin\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AdminModel\Announcement;
use Validator;
use App\AdminModel\Batch;
use App\AdminModel\Student;
use App\AdminModel\AnnouncementRoll;

class AnnouncementController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $announcementDetails = Announcement::with('announcements.roll')->orderby('id','desc')->get();
        // dd($announcementDetails);
        return view('admin.content.announcements.index',compact("announcementDetails"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $studentBatchs =  Batch::orderBy('id','desc')->get();
       return view ('admin.content.announcements.create',compact('studentBatchs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $announcementsData = Validator::make($request->all(),[
            "text" => "required"
        ],[

            "text.required" => "Enter Your Announcements"
        ])->validate();

        $announcementsData                  =  new Announcement;
        $announcementsData->announcement    =  $request->text;
        if($request->hasFile('image')){
            $announcementsData->image       =  'storage/'.$request->file('image')->store('announcements/'.date('Y/m/d'));
        }
        $announcementsData->batch           =   $request->batch;
        $announcementsData->time            =   time();
        $announcementsData->save();

        foreach($request->rolls as $roll){ 
            $announcementRoll              =  new AnnouncementRoll;
            $announcementRoll->announce_id =  $announcementsData->id;
            $announcementRoll->roll_id     =  $roll;
            $announcementRoll->save();
        }
        return redirect()->route('announcement.list')->with('success','Announcements Created Successfully');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $announcementsEdit = Announcement::find($id);
        return view('admin.content.announcements.edit')->with('announcementsEdit',$announcementsEdit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $announcementsData      =   Validator::make($request->all(),[
            "announcements_id"  =>  "required|numeric",
            "text"              =>  "required"
        ],[

            "text.required"     =>  "Enter Your Announcements"
        ])->validate();


        $announcementsData                  =  Announcement::find($request->announcements_id);
        $announcementsData->announcement    =  $request->text;
        if($request->hasFile('image')){
            $announcementsData->image       =  'storage/'.$request->file('image')->store('announcements/'.date('Y/m/d'));
        }
        $announcementsData->time            =   time();
        $announcementsData->save();
        return redirect()->route('announcement.list')->with('success','Announcements Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $announcementsDelete = Announcement::find($id);
        $announcementsDelete -> delete();
        return redirect()->route('announcement.list')->with('success','Announcements Deleted Successfully');
    }

    public function getRoll(Request $request){

        $getroll= Student::where('batch',$request->batch_id)->get();

        if(count($getroll)>0){
            $response['getroll'] = $getroll;
            return response()->json($response,200);
        }else{
            $reponse['error'] = "Nothing Found";
            return response()->json($response,400);
        }
    }
}
