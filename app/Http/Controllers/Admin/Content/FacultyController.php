<?php

namespace App\Http\Controllers\Admin\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AdminModel\Faculty;
use Validator;

class FacultyController extends Controller
{

   public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $facultyDetails = Faculty::orderby('id','desc')->get();
        return view('admin.content.facultys.index',compact("facultyDetails"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.content.facultys.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(),[
            "name"    =>  "required",
            "subject" =>  "required",
            "class"   =>  "required",

        ])->validate();

        $facultyData          =  new Faculty();
        $facultyData->name    =  $request->name;
        $facultyData->subject =  $request->subject;
        $facultyData->class   =  $request->class;
        $facultyData->time    =  time();
        $facultyData->save();
        return redirect()->route('faculty.list')->with('success','Faculty Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $facultyEdit = Faculty::find($id);
        return view('admin.content.facultys.edit')->with('facultyEdit',$facultyEdit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       
        Validator::make($request->all(),[
            "faculty_id"  =>  "required|numeric",
            "name"        =>  "required",
            "subject"     =>  "required",
            "class"       =>  "required",

        ])->validate();

        $facultyData          =  Faculty::find($request->faculty_id);
        $facultyData->name    =  $request->name;
        $facultyData->subject =  $request->subject;
        $facultyData->class   =  $request->class;
        $facultyData->time    =  time();
        $facultyData->save();
        return redirect()->route('faculty.list')->with('success','Faculty Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $facultyDelete = Faculty::find($id);
        $facultyDelete -> delete();
        return redirect()->route('faculty.list')->with('success','Faculty Deleted Successfully');
    }
}
