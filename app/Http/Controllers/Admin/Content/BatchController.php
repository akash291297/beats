<?php

namespace App\Http\Controllers\Admin\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AdminModel\StudentClass;
use App\AdminModel\Subject;
use App\AdminModel\Weekday;
use App\AdminModel\Batch;
use App\AdminModel\BatchWeek;
use Validator;

class BatchController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $batchDetails = Batch::with('batchweek.weekday','class','subject')->orderBy('id','desc')->get();
        return view('admin.content.batches.index',compact('batchDetails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $weekLists  = Weekday::orderBy('id','asc')->get();
        $classLists = StudentClass::orderBy('id','asc')->get();
        return view('admin.content.batches.create',compact('classLists','weekLists'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(),[
                "name"        =>  "required",
                "class_id"    =>  "required",
                "subject_id"  =>  "required",
                "center"      =>  "required",
                "date"        =>  "required",
                "stime"       =>  "required",
                "etime"       =>  "required",
                "week"        =>  "required"
        ],[
                "name.required"     =>   "Enter Batch Name",
                "class_id.required" =>   "Select Class",
                "subject_id"        =>   "Choose Subject",
                "center"            =>   "Choose Center",
                "date.required"     =>   "Choose Date",
                "stime.required"    =>   "Select Batch Start Time",
                "etime.required"    =>   "Select Batch End Time",
                "week.required"     =>   "Select Weekday"   
        ])->validate();

        $batchData               =   new Batch;
        $batchData->name         =   $request->name;
        $batchData->class_id     =   $request->class_id;
        $batchData->subject_id   =   $request->subject_id;
        $batchData->center       =   $request->center;
        $batchData->date         =   strtotime($request->date);
        $batchData->starttime    =   strtotime($request->stime);
        $batchData->endtime      =   strtotime($request->etime);
        $batchData->time         =   time();
        $batchData->save();


        foreach($request->week as $we){
            $weekData = new BatchWeek;
            $weekData->batch_id  =  $batchData->id;
            $weekData->week_id   =  $we;
            $weekData->time   =  time();
            $weekData->save();
        }
        return redirect()->route('batch.list')->with('success','Batch Created Successfully');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $weekLists  = Weekday::orderBy('id','asc')->get();
        $classLists = StudentClass::orderBy('id','asc')->get();
        $batchEdit  = Batch::selectRaw('addbatch.*,(select group_concat(week_id)from batch_week where batch_id = addbatch.id) as week')->find($id);
        return view('admin.content.batches.edit',compact('classLists','weekLists'))->with('batchEdit',$batchEdit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        Validator::make($request->all(),[
                "batch_id"    =>  "required|numeric",
                "name"        =>  "required",
                "class_id"    =>  "required",
                "subject_id"  =>  "required",
                "center"      =>  "required",
                "date"        =>  "required",
                "stime"       =>  "required",
                "etime"       =>  "required",
                "week"        =>  "required"
        ],[
                "name.required"     =>   "Enter Batch Name",
                "class_id.required" =>   "Select Class",
                "subject_id"        =>   "Choose Subject",
                "center"            =>   "Choose Center",
                "date.required"     =>   "Choose Date",
                "stime.required"    =>   "Select Batch Start Time",
                "etime.required"    =>   "Select Batch End Time",
                "week.required"     =>   "Select Weekday"   
        ])->validate();

        $batchData               =   Batch::find($request->batch_id);
        $batchData->name         =   $request->name;
        $batchData->class_id     =   $request->class_id;
        $batchData->subject_id   =   $request->subject_id;
        $batchData->center       =   $request->center;
        $batchData->date         =   strtotime($request->date);
        $batchData->starttime    =   strtotime($request->stime);
        $batchData->endtime      =   strtotime($request->etime);
        $batchData->time         =   time();
        $batchData->save();

        BatchWeek::where('batch_id',$batchData->id)->delete();
        foreach($request->week as $we){
            $weekData = new BatchWeek;
            $weekData->batch_id  =  $batchData->id;
            $weekData->week_id   =  $we;
            $weekData->time      =  time();
            $weekData->save();
        }
        return redirect()->route('batch.list')->with('success','Batch Created Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $allData = Batch::find($id);
        $allData->delete();
        return redirect()->route('batch.list')->with('status','Deleted Successfully');
    }

    public function getSubject(Request $request){

        $getsubject= Subject::where('class_id',$request->class_id)->get();
        if(count($getsubject)>0){
            $response['getsubject'] = $getsubject;
            return response()->json($response,200);
        }else{
            $reponse['error'] = "Nothing Found";
            return response()->json($response,400);
        }
    }
}
