<?php

namespace App\Http\Controllers\Admin\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AdminModel\Student;
use App\AdminModel\Batch;
use App\AdminModel\StudentClass;
use Validator;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $studentDetails = Student::orderby('id','desc')->get();
        return view('admin.content.student.index',compact("studentDetails"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $batchDetails = Batch::orderBy('id','desc')->get();
        $classDetails = StudentClass::orderBy('id','asc')->get();
        return view ('admin.content.student.create',compact('batchDetails','classDetails'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $studentData    =   Validator::make($request-> all(),[
            "roll"      =>  "required|numeric",
            "name"      =>  "required",
            "class"     =>  "required|numeric",
            "school"    =>  "required",
            "batch"     =>  "required",
            "address"   =>  "required",
            "fname"     =>  "required",
            "mname"     =>  "required",
            "fcontact"  =>  "required|min:10|numeric|unique:students,fcontact",
            "mcontact"  =>  "required|min:10|numeric|unique:students,mcontact",
            "scontact"  =>  "required|min:10|numeric|unique:students,scontact"
        ],[
            "roll.required"       =>   "Enter Student Roll No",
            "name.required"       =>   "Enter The Student Name",
            "name.min"            =>   "Minimum 5 Character Name",
            "class.required"      =>   "Enter The Student Class",
            "school.required"     =>   "Enter the Student School Name",
            "batch.required"      =>   "Enter the Student Batch",
            "text.required"       =>   "Enter the Address",
            "fname.required"      =>   "Enter the Father Name",
            "mname.required"      =>   "Enter the Mother Name",
            "fcontact.required"   =>   "Enter the Father Contact",
            "mcontact.required"   =>   "Enter the Mother Contact",
            "scontact.required"   =>   "Enter the Student Contact",

        ])->validate();
        
        $studentData          =  new Student;
        $studentData->roll_no =  $request->roll;
        $studentData->name    =  $request->name;
        $studentData->class   =  $request->class;
        $studentData->school  =  $request->school;
        if($request->hasFile('image')){
         $studentData->profilepic =   'storage/'.$request->file('image')->store('students/'.date('Y/m/d'));
        }
        
        $studentData->batch     =  $request->batch;
        $studentData->address   =  $request->address;
        $studentData->father    =  $request->fname;
        $studentData->mother    =  $request->mname;
        $studentData->fcontact  =  $request->fcontact;
        $studentData->mcontact  =  $request->mcontact;
        $studentData->scontact  =  $request->scontact;
        $studentData->time      =  time();
        $studentData->save();
        return redirect()->route('student.list')->with('success','Student Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $studentEdit  = Student::find($id);
        $batchDetails = Batch::orderBy('id','desc')->get();
        $classDetails = StudentClass::orderBy('id','asc')->get();
        return view('admin.content.student.edit',compact('classDetails','batchDetails'))->with('studentEdit',$studentEdit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $studentData=Validator::make($request-> all(),[

            "student_id"=>  "required|numeric",
            "name"      =>  "required",
            "class"     =>  "required|numeric",
            "school"    =>  "required",
            "batch"     =>  "required",
            "address"   =>  "required",
            "fname"     =>  "required",
            "mname"     =>  "required",
            "fcontact"  =>  "required|min:10|numeric",
            "mcontact"  =>  "required|min:10|numeric",
            "scontact"  =>  "required|min:10|numeric"
        ],[

            "name.required"       =>   "Enter The Student Name",
            "name.min"            =>   "Minimum 5 Character Name",
            "class.required"      =>   "Enter The Student Class",
            "school.required"     =>   "Enter the Student School Name",
            "batch.required"      =>   "Enter the Student Batch",
            "text.required"       =>   "Enter the Address",
            "fname.required"      =>   "Enter the Father Name",
            "mname.required"      =>   "Enter the Mother Name",
            "fcontact.required"   =>   "Enter the Father Contact",
            "mcontact.required"   =>   "Enter the Mother Contact",
            "scontact.required"   =>   "Enter the Student Contact",

        ])->validate();

        $studentData          =  Student::find($request->student_id);
        $studentData->name    =  $request->name;
        $studentData->class   =  $request->class;
        $studentData->school  =  $request->school;
        if($request->hasFile('image')){
         $studentData->profilepic =   'storage/'.$request->file('image')->store('images/'.date('Y/m/d'));
        }
        
        $studentData->batch     =  $request->batch;
        $studentData->address   =  $request->address;
        $studentData->father    =  $request->fname;
        $studentData->mother    =  $request->mname;
        $studentData->fcontact  =  $request->fcontact;
        $studentData->mcontact  =  $request->mcontact;
        $studentData->scontact  =  $request->scontact;
        $studentData->time      =  time();
        $studentData->save();
        return redirect()->route('student.list')->with('success','Student Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteStudent = Student::find($id);
        $deleteStudent -> delete();
        return redirect()->route('student.list')->with('success','Student Deleted Successfully');
    }
}
