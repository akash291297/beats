<?php

namespace App\Http\Controllers\Admin\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\AdminModel\AdminProfile;
class ProfileController extends Controller
{
    // public function profile(){
    // 	return view ('admin.content.profile.create');
    // }

    public function index(){
    	$adminList = AdminProfile::orderBy('id','desc')->first();
    	return view('admin.content.profile.create',compact('adminList'));
    }

    public function store(Request $request){
    	Validator::make($request->all(),[
    		"name"          => "required",
    		"email"         => "required|email|unique:adminprofiles,email",
    		"qualification" => "required"
    	],[
    		"name.required"  => "Enter Your Name",
    		"email.required" => "Enter Your Email Id",
    		"email.unique"   => "Email Should be unique"
    	])->validate();

    	$adminProfile                = new AdminProfile();
    	$adminProfile->name          = $request->name;
    	$adminProfile->email         = $request->email;
    	if($request->hasFile('image')){
    		$adminProfile->image = 'storage/'.$request->file('image')->store('adminProfile/'.date('Y/m/d'));
    	}
    	$adminProfile->qualification = $request->qualification;
    	$adminProfile->time = time();
    	$adminProfile->save();
    	return redirect()->route('admin.profile')->with('success','Admin Profile Created Successfully');
	}

	public function edit($id){
		$adminEdit = AdminProfile::find($id);
		return view('admin.content.profile.create')->with('adminEdit',$adminEdit);
	}

	public function update(Request $request){
		
		Validator::make($request->all(),[
			"profile_id"    => "required|numeric",
    		"name"          => "required",
    		"email"         => "required|email",
    		"qualification" => "required"
    	],[
    		"name.required"  => "Enter Your Name",
    		"email.required" => "Enter Your Email Id",
    		"email.unique"   => "Email Should be unique"
    	])->validate();

    	$adminProfile                = AdminProfile::find($request->profile_id);
    	$adminProfile->name          = $request->name;
    	$adminProfile->email         = $request->email;
    	if($request->hasFile('image')){
    		$adminProfile->image = 'storage/'.$request->file('image')->store('adminProfile/'.date('Y/m/d'));
    	}
    	$adminProfile->qualification = $request->qualification;
    	$adminProfile->save();
    	return redirect()->route('admin.profile')->with('success','Admin Profile Updated Successfully');
	}
}
