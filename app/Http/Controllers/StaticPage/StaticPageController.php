<?php

namespace App\Http\Controllers\StaticPage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StaticPageController extends Controller
{
   public function slider(){
   	return view('master.center');
   }

   public function about(){
   	return view('master.aboutus');
   }

   public function admission(){
   	return view('master.admission');
   }



}
