<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\StudentAdmin;
use Validator,Auth;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:student')->except('logout');
    }
    
    public function login(Request $request){
        Validator::make($request->all(),[
            "roll_no"  =>  "required|numeric",
            "email"    =>  "required|email",
            "password" =>  "required|max:10"
        ])->validate();


        if(Auth::guard('student')->attempt($request->only('roll_no','email','password'))){
            return redirect()->route('student.dashboard')->with('success','Login Successfully');
        }
    }

    protected function guard()
    {
        return Auth::guard('student');
    }
}
