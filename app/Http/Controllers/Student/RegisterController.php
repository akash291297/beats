<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\StudentAdmin;
use Validator;


class RegisterController extends Controller
{
    public function register(Request $request){
    	Validator::make($request->all(),[
            "roll"     =>  "required|numeric",
    		"name"     =>  "required",
    		"email"    =>  "required|email|unique:studentadmins,email",
    		"password" =>  "required",
    		"retype"   =>  "required|same:password"
     	])->validate();

     	$studentRegister           =   new StudentAdmin();
        $studentRegister->roll_no  =   $request->roll;
     	$studentRegister->name     =   $request->name;
     	$studentRegister->email    =   $request->email;
     	$studentRegister->password =   bcrypt($request->password);
     	$studentRegister->time     =   time();
     	$studentRegister->save();
     	$notification = array(
			'message' => 'You Are Logged In Successfully!!', 
			'alert-type' => 'success'
		);
     	return redirect()->route('student.dashboard')->with($notification);

    }
}
