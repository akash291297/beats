<?php

namespace App\Http\Controllers\Student\content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AdminModel\Student;
use App\AdminModel\Announcement;
use App\AdminModel\Assignment;
use App\AdminModel\AnnouncementRoll;
use Auth;
use DB;
class MyDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $myDetails =  Student::where('roll_no',Auth::user()->roll_no)->orderBy('id','desc')->get();
        return view('studentadmin.content.mydetails.index',compact('myDetails'));
    }

    public function announcement()
    {
        $myAnnouncements =  DB::table('announcements')
                            ->join('announcement_roll', 'announcements.id', '=', 'announcement_roll.announce_id')
                            ->join('students', 'announcement_roll.roll_id', '=', 'students.roll_no')
                            ->where('roll_no','=',Auth::user()->roll_no)
                            ->select('announcements.*','students.roll_no')
                            ->get();
        return view('studentadmin.content.announcement.index',compact('myAnnouncements'));
    }

    public function assignment()
    {
        $myAssignments =  Assignment::where('id',Auth::user()->id)->orderBy('id','desc')->get();
        return view('studentadmin.content.assignment.index',compact('myAssignments'));
    }



    // problem in finding or selecting student model for auth::user()->roll_no
    // SELECT * FROM `announcements` INNER JOIN announcement_roll ON announcements.id = announcement_roll.announce_id INNER JOIN students ON announcement_roll.roll_id = students.roll_no WHERE roll_no = 1

    
}
