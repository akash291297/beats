<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ViewController extends Controller
{
    public function loginPage()
    {
        return view('studentadmin.login');
    }

    public function registerPage()
    {
        return view('studentadmin.register');
    }
}
