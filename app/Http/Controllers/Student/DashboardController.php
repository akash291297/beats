<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
class DashboardController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth:student');
    }

    public function dashboard()     
    {
        return view('studentlayouts.main');
    }

    public function logout(Request $request){
    	Auth::guard('student')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        $notification = array(
            'message' => 'You Are Logged Out Successfully!!', 
            'alert-type' => 'success'
        );
        return redirect()->route('student.login')->with($notification);

    }
}
