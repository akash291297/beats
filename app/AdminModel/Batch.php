<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Batch extends Model
{
    use SoftDeletes;
    protected $table = 'addbatch';

    public function batchweek()
    {
    	return $this->hasMany('App\AdminModel\BatchWeek','batch_id');
    }

    public function class(){
    	return $this->belongsTo(StudentClass::class,'class_id','id');
    }

    public function subject(){
    	return $this->belongsTo(Subject::class,'subject_id','id');
    }
}
