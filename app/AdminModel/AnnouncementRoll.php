<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;

class AnnouncementRoll extends Model
{
    protected $table = 'announcement_roll';

    public $timestamps = false;

    public function roll(){
    	return $this->belongsTo(Student::class,'roll_id','roll_no');
    }
}
