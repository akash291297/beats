<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Announcement extends Model
{
    use SoftDeletes;
    protected $table = 'announcements';

    public function announcements(){
    	return $this->hasMany('App\AdminModel\AnnouncementRoll','announce_id');
    }
}
