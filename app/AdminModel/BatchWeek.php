<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BatchWeek extends Model
{
    use SoftDeletes;
    protected $table = 'batch_week';

    public function weekday(){
    	return $this->belongsTo(Weekday::class,'week_id','id');
    }
}
