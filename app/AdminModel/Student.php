<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Student extends Model
{
    use SoftDeletes;
    protected $table = 'students';

    public function announced(){
    	return $this->hasMany(AnnouncementRoll::class,'roll_no','id');
    }
    
}
