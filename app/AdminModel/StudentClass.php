<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentClass extends Model
{
    use SoftDeletes;
    protected $table = 'studentclass';

    public function subject(){
    	return $this->hasMany(Subject::class,'id','class_id');
    }

    public function batch(){
    	return $this->hasMany(Batch::class,'id','class_id');
    }
}
