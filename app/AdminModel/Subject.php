<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subject extends Model
{
    use SoftDeletes;
    protected $table = 'subjects';

    public function class(){
    	return $this->belongsTo(StudentClass::class,'class_id','id');
    }

    public function batch(){
    	return $this->hasMany(Batch::class,'id','subject_id');
    }
}
