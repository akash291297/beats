<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class StudentAdmin extends Authenticatable
{
    use SoftDeletes;
    protected $table = "studentadmins";
    protected $guard = 'student';
}
