
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset('img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->name}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview menu-open">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
          </li>
           <li class=" treeview">
          <a href="#">
            <i class=" fa-calendar-check-o"></i> <span>Time Table</span>
          </a>
          </li>
        
        <li >
          <a href="{{route('details')}}">
            <i class="fa fa-share"></i> <span>My Details</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>
          <!-- <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Create Student</a></li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> List Student
              </a>
            </li>
           </ul> -->
          </li>
         <li>
          <a href="{{route('announcement')}}">
            <i class="fa fa-share"></i> <span>Announcements</span>
           <!--  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>
          <!-- <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Create Announcement</a></li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> List Announcement
              </a>
            </li>
           </ul> -->
          </li>
        <li>
          <a href="{{route('assignment')}}">
            <i class="fa fa-share"></i> <span>Assignments</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <!-- <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Create Assignment</a></li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> List Assignment
              </a>
            </li>
           </ul> -->
          </li>
           <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Batches</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <!-- <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Create Batch</a></li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> List Batch
              </a>
            </li>
           </ul> -->
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Classes</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <!-- <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Create Class</a></li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> List Class
              </a>
            </li>
           </ul> -->
        </li>
      <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Faculties</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <!-- <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Create Faculty</a></li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> List Faculty
              </a>
            </li>
           </ul> -->
        </li>
      </ul>
   