@extends('master.welcome')
@section('content')
<!-- SLIDER -->
     <section>
        <div class="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item slider1 active">
                    @if(count(staticpage())>0)
                        @foreach(staticpage() as $static)
                            <img src="{{asset($static->banner)}}"  alt="">
                        @endforeach
                    @endif
                   
                </div>
                <div class="item slider2">
                    @if(count(staticpage())>0)
                        @foreach(staticpage() as $static)
                            <img src="{{asset($static->banner1)}}"  alt="">
                        @endforeach
                    @endif
                    
                </div>
                <div class="item">
                    @if(count(staticpage())>0)
                        @foreach(staticpage() as $static)
                            <img src="{{asset($static->banner2)}}"  alt="">
                        @endforeach
                    @endif
                    
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <i class="fa fa-chevron-left slider-arr"></i>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <i class="fa fa-chevron-right slider-arr"></i>
            </a>
        </div>
    </section>

    <!-- QUICK LINKS -->
    <section>
        <div class="container">
            <div class="row">
                <div class="wed-hom-ser">
                    
                </div>
            </div>
        </div>
    </section>

    <!-- DISCOVER MORE -->
    <section>
        <div class="container com-sp pad-bot-70">
            <div class="row">
                <div class="con-title">
                    <h2>Discover <span>More</span></h2>
                    <p>Fusce id sem at ligula laoreet hendrerit venenatis sed purus. Ut pellentesque maximus lacus, nec pharetra augue.</p>
                </div>
            </div>
            <div class="row">
                <div class="ed-course">
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="ed-course-in">
                            <a class="course-overlay" href="{{route('page.about')}}">
                                <img src="{{asset('education/images/h-about.jpg')}}" alt="">
                                <span>Academics</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="ed-course-in">
                            <a class="course-overlay" href="{{route('page.admission')}}">
                                <img src="{{asset('education/images/h-adm1.jpg')}}" alt="">
                                <span>Admission</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="ed-course-in">
                            <a class="course-overlay" href="dashboard.html">
                                <img src="{{asset('education/images/h-cam.jpg')}}" alt="">
                                <span>Students profile</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="ed-course-in">
                            <a class="course-overlay" href="research.html">
                                <img src="{{asset('education/images/h-res.jpg')}}" alt="">
                                <span>Research & Education</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="ed-course-in">
                            <a class="course-overlay" href="all-courses.html">
                                <img src="{{asset('education/images/h-about1.jpg')}}" alt="">
                                <span>Couse</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="ed-course-in">
                            <a class="course-overlay" href="db-time-line.html">
                                <img src="{{asset('education/images/h-adm.jpg')}}" alt="">
                                <span>Exam Time Line</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="ed-course-in">
                            <a class="course-overlay" href="seminar.html">
                                <img src="{{asset('education/images/h-cam1.jpg')}}" alt="">
                                <span>Seminar 2018</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="ed-course-in">
                            <a class="course-overlay" href="events.html">
                                <img src="{{asset('education/images/h-res1.jpg')}}" alt="">
                                <span>Research & Education</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- POPULAR COURSES -->
     <section class="pop-courses">
       <div class="container com-sp pad-bot-70">
           <div class="row">
               <div class="con-title">
                   <h2>Free Popular <span>Courses</span></h2>
                   <p>Fusce id sem at ligula laoreet hendrerit venenatis sed purus. Ut pellentesque maximus lacus, nec pharetra augue.</p>
               </div>
           </div>
           <div class="row">
               <div class="col-md-4 col-sm-4">
                    <div class="border rounded" style="background-color: #E696DD;width: 361px;height: 443px;text-transform: uppercase;color: #fff;border-radius: 25px;">
                            <h5 class="text-center" style="font-size: 20px;margin-bottom: 8px;">CLASSES 8-10</h5>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                        <div class="col-md-3">    
                                            <div style="padding-left: 15px;">CBSE</div>
                                        </div>
                                        <div class="col-md-3 ">    
                                            <div style="padding-left: 15px;">ICSE</div>
                                        </div>
                                        <div class="col-md-5">    
                                            <div style="padding-left: 15px;">STATE BOARD</div>
                                        </div>

                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="text-center" style="font-size: 13px;color: white;">Focus on Conceptual learning with personalised learning programs</p>
                                            <br><br>
                                    <ul style="margin-bottom: 10px;padding-left: 12px;">
                                        <li style="color: white;">Mastery of concepts through personalized learning journeys</li><br>
                                        <li style="color: white">Engaging video lessons from India's best teachers</li><br>
                                        <li style="color: white">Application and simulation games to guide experiential learning</li>
                                    </ul>
                                    <button class=" btn rounded" style="background-color: #9456FF;position: absolute;bottom: -95px;left: 30%;">Get Started</button>
                                </div>
                            </div>
                    </div>
               </div>
               <div class="col-md-4 col-sm-4">
                   <div class="border rounded" style="background-color: #FFAA7E;width: 361px;height: 443px;border-radius: 25px;text-transform: uppercase;color: #fff;">
                       <h5 class="text-center" style="font-size: 20px;margin-bottom: 8px;">CLASSES 11-12</h5>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                        <div class="col-md-3 offset-md-3">    
                                            <div style="padding-left: 15px;">CBSE</div>
                                        </div>
                                        
                                        <div class="col-md-5">    
                                            <div style="padding-left: 15px;">STATE BOARD</div>
                                        </div>

                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="text-center" style="font-size: 13px;color: white">Focus on Conceptual learning with personalised learning programs</p>
                                            <br><br>
                                    <ul style="list-style-type: none;margin-bottom: 10px;padding-left: 12px;">
                                        <li style="color: white;">Mastery of concepts through personalized learning journeys</li><br>
                                        <li style="color: white">Engaging video lessons from India's best teachers</li><br>
                                        <li style="color: white">Application and simulation games to guide experiential learning</li>
                                    </ul>
                                    <button class=" btn rounded" style="background-color: #9456FF;position: absolute;bottom: -95px;left: 35%;">Get Started</button>
                                </div>
                            </div>
                   </div>
               </div>
               <div class="col-md-4 col-sm-4 mt-5">
                   <div class="border rounded" style="background-color: #4ACFE8;width: 361px;height: 443px;border-radius: 25px;text-transform: uppercase;color: #fff;">
                       <h5 class="text-center" style="font-size: 20px;margin-bottom: 8px;">JEE | NEET | CA-FOUNDATION</h5>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="text-center" style="font-size: 13px;color: white">Focus on Conceptual learning with personalised learning programs</p>
                                            <br><br>
                                    <ul style="margin-bottom: 10px;padding-left: 12px;">
                                        <li style="color: white;">Mastery of concepts through personalized learning journeys</li><br>
                                        <li style="color: white">Engaging video lessons from India's best teachers</li><br>
                                        <li style="color: white">Application and simulation games to guide experiential learning</li>
                                    </ul>
                                    <button class=" btn rounded" style="background-color: #9456FF;position: absolute;bottom: -115px;left: 35%;">Get Started</button>
                                </div>

                            </div>
                   </div>
               </div>
           </div>
       </div>
   </section>  

    <!-- UPCOMING EVENTS -->
    <section>
        <div class="container com-sp pad-bot-0">
            <div class="row">
                <div class="col-md-4">
                    <!--<div class="ho-ex-title">
                            <h4>Upcoming Event</h4>
                        </div>-->
                    <div class="ho-ev-latest ho-ev-latest-bg-1">
                        <div class="ho-lat-ev">
                            <h4>Upcoming Event</h4>
                            <p>Nulla at velit convallis, venenatis lacus quis, efficitur lectus.</p>
                        </div>
                    </div>
                    <div class="ho-event ho-event-mob-bot-sp">
                        <ul>
                            <li>
                                <div class="ho-ev-date"><span>07</span><span>jan,2018</span>
                                </div>
                                <div class="ho-ev-link">
                                    <a href="events.html">
                                        <h4>Latinoo College Expo 2018</h4>
                                    </a>
                                    <p>Nulla at velit convallis, venenatis lacus quis, efficitur lectus.</p>
                                    <span>9:15 am – 5:00 pm</span>
                                </div>
                            </li>
                            <li>
                                <div class="ho-ev-date"><span>12</span><span>jan,2018</span>
                                </div>
                                <div class="ho-ev-link">
                                    <a href="events.html">
                                        <h4>Training at Team Fabio Clemente</h4>
                                    </a>
                                    <p>Nulla at velit convallis venenatis.</p>
                                    <span>9:15 am – 5:00 pm</span>
                                </div>
                            </li>
                            <li>
                                <div class="ho-ev-date"><span>26</span><span>jan,2018</span>
                                </div>
                                <div class="ho-ev-link">
                                    <a href="events.html">
                                        <h4>Nulla at velit convallis</h4>
                                    </a>
                                    <p>Nulla at velit convallis, venenatis lacus quis, efficitur lectus.</p>
                                    <span>9:15 am – 5:00 pm</span>
                                </div>
                            </li>
                            <li>
                                <div class="ho-ev-date"><span>18</span><span>jan,2018</span>
                                </div>
                                <div class="ho-ev-link">
                                    <a href="events.html">
                                        <h4>Admissions Information Session and Tour</h4>
                                    </a>
                                    <p>Nulla at velit convallis, venenatis lacus quis, efficitur lectus.</p>
                                    <span>9:15 am – 5:00 pm</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <!--<div class="ho-ex-title">
                            <h4>Upcoming Event</h4>
                        </div>-->
                    <div class="ho-ev-latest ho-ev-latest-bg-2">
                        <div class="ho-lat-ev">
                            <h4>Job Vacants</h4>
                            <p>Nulla at velit convallis, venenatis lacus quis, efficitur lectus.</p>
                        </div>
                    </div>
                    <div class="ho-event ho-event-mob-bot-sp">
                        <ul>
                            <li>
                                <div class="ho-ev-img"><img src="images/event/1.jpg" alt="">
                                </div>
                                <div class="ho-ev-link">
                                    <a href="#">
                                        <h4>Almost before we knew it, we had left the ground</h4>
                                    </a>
                                    <p>Etiam ornare lacus nec lectus vestibulum aliquam.</p>
                                    <span>Location: New York</span>
                                </div>
                            </li>
                            <li>
                                <div class="ho-ev-img"><img src="images/event/2.jpg" alt="">
                                </div>
                                <div class="ho-ev-link">
                                    <a href="#">
                                        <h4>Then came the night of the first falling star.</h4>
                                    </a>
                                    <p>Vestibulum sollicitudin sem arcu</p>
                                    <span>Location: Los Angeles</span>
                                </div>
                            </li>
                            <li>
                                <div class="ho-ev-img"><img src="images/event/3.jpg" alt="">
                                </div>
                                <div class="ho-ev-link">
                                    <a href="#">
                                        <h4>Educate to Empower NYE Party</h4>
                                    </a>
                                    <p>Vestibulum sollicitudin sem arcu, eget ullamcorper purus hendrerit</p>
                                    <span>Location: Chennai</span>
                                </div>
                            </li>
                            <li>
                                <div class="ho-ev-img"><img src="images/event/4.jpg" alt=""></div>
                                <div class="ho-ev-link">
                                    <a href="#">
                                        <h4>Then came the night of the first falling star.</h4>
                                    </a>
                                    <p>Venenatis lacus lectus.</p>
                                    <span>Location: Chicago</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <!--<div class="ho-ex-title">
                            <h4>Upcoming Event</h4>
                        </div>-->
                    <div class="ho-ev-latest ho-ev-latest-bg-3">
                        <div class="ho-lat-ev">
                            <h4>Register & Login</h4>
                            <p>Student area velit convallis venenatis lacus quis, efficitur lectus.</p>
                        </div>
                    </div>
                    <div class="ho-st-login">
                        <ul class="tabs tabs-hom-reg">
                            <li class="tab col s6"><a href="#hom-vijay">Register</a>
                            </li>
                            <li class="tab col s6"><a href="#hom_log">Login</a>
                            </li>
                        </ul>
                        <div id="hom-vijay" class="col s12">
                            <form class="col s12">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input type="text" class="validate">
                                        <label>User name</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input type="text" class="validate">
                                        <label>Email id</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input type="password" class="validate">
                                        <label>Password</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input type="password" class="validate">
                                        <label>Confirm password</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input type="submit" value="Register" class="waves-effect waves-light light-btn">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div id="hom_log" class="col s12">
                            <form class="col s12">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input type="text" class="validate">
                                        <label>Student user name</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input type="text" class="validate">
                                        <label>Password</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input type="submit" value="Login" class="waves-effect waves-light light-btn">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- NEWS AND EVENTS -->
    <section>
        <div class="container com-sp">
            <div class="row">
                <div class="con-title">
                    <h2>Our Esteemed <span>Faculty</span></h2>
                    <p>Fusce id sem at ligula laoreet hendrerit venenatis sed purus. Ut pellentesque maximus lacus, nec pharetra augue.</p>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2 col-sm-2">
                        <img src="{{asset('teachers/anshu.png')}}" class="img-fluid" style="width: 170px;" alt="">
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <img src="{{asset('teachers/birender.png')}}" class="img-fluid" style="width: 170px;" alt="">
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <img src="{{asset('teachers/chandan.png')}}" class="img-fluid" style="width: 170px;" alt="">
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <img src="{{asset('teachers/dharma.png')}}" class="img-fluid" style="width: 170px;" alt="">
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <img src="{{asset('teachers/Nikunj.png')}}" class="img-fluid" style="width: 170px;" alt="">
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <img src="{{asset('teachers/sumit.png')}}" class="img-fluid" style="width: 170px;" alt="">
                    </div>
                </div>
            </div>
    </section>
@endsection
