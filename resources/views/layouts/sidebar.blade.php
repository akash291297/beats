
      <div class="user-panel">
        <div class="pull-left image">
            @if(count(profile())>0)
                @foreach(profile() as $pr)
                <img src="{{asset($pr->image)}}" class="user-image img-circle" alt="User Image">
                @endforeach
            @endif
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->name}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class=" treeview menu-open">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
          </li>
           <li>
          <a href="{{route('timetable')}}">
            <i class="fa fa-calendar-check-o"></i> <span>Time Table</span>
          </a>
          </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-child"></i> <span>Add Student</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('student.create')}}"><i class="fa fa-circle-o"></i> Create Student</a></li>
            <li>
              <a href="{{route('student.list')}}"><i class="fa fa-circle-o"></i> List Student
              </a>
            </li>
           </ul>
          </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-bullhorn"></i> <span>Add Announcement</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('announcement.create')}}"><i class="fa fa-circle-o"></i> Create Announcement</a></li>
            <li>
              <a href="{{route('announcement.list')}}"><i class="fa fa-circle-o"></i> List Announcement
              </a>
            </li>
           </ul>
          </li>
        <li class="treeview">
          <a href="#">
            <i class="fa  fa-file"></i> <span>Add Assignment</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('assignment.create')}}"><i class="fa fa-circle-o"></i> Create Assignment</a></li>
            <li>
              <a href="{{route('assignment.list')}}"><i class="fa fa-circle-o"></i> List Assignment
              </a>
            </li>
           </ul>
          </li>
           <li class="treeview">
          <a href="#">
            <i class="fa  fa-group"></i> <span>Add Batch</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('batch.create')}}"><i class="fa fa-circle-o"></i> Create Batch</a></li>
            <li>
              <a href="{{route('batch.list')}}"><i class="fa fa-circle-o"></i> List Batch
              </a>
            </li>
           </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-institution"></i> <span>Add Class</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('class.create')}}"><i class="fa fa-circle-o"></i> Create Class</a></li>
            <li>
              <a href="{{route('class.list')}}"><i class="fa fa-circle-o"></i> List Class
              </a>
            </li>
           </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-pencil-square-o"></i> <span>Add Subject</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('subject.create')}}"><i class="fa fa-circle-o"></i> Create Subject</a></li>
            <li>
              <a href="{{route('subject.list')}}"><i class="fa fa-circle-o"></i> List Subject
              </a>
            </li>
           </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Add Faculty</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
           <ul class="treeview-menu">
            <li><a href="{{route('faculty.create')}}"><i class="fa fa-circle-o"></i> Create Faculty</a></li>
            <li>
              <a href="{{route('faculty.list')}}"><i class="fa fa-circle-o"></i> List Faculty
              </a>
            </li>
            </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-sticky-note-o"></i> <span>Static Pages</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
           <ul class="treeview-menu">
            <li><a href="{{route('static.create')}}"><i class="fa fa-circle-o"></i> Create Pages</a></li>
            <li>
              <a href="{{route('static.list')}}"><i class="fa fa-circle-o"></i> List Pages
              </a>
            </li>
            </ul>
        </li>
      </ul>
   