@extends('layouts.app')
@section('content')

	<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>BEATS</b>Forgot</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <form action="{{route('user.login')}}" method="post">
      @csrf
      <div class="form-group has-feedback">
        <input type="email" class="form-control" name="email" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      
      <div class="row">
        <div class="col-xs-8">
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
        </div>
        <!-- /.col -->
      </div>
      
    </form>
</div>
  <a href="{{route('admin.register')}}" class="text-center">Register a new membership</a>
  <a href="{{route('admin.forgot')}}" class="nav-link">ForgotPassword</a>
</div>
<!-- /.login-box -->


@endsection