@extends('layouts.main')
@section('dynamic')
<section class="content"> 
    <div class="row">
        <!-- left column -->
    <div class="col-md-6 col-md-offset-3">
  <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Create Faculty</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{route('faculty.update')}}" method="post" enctype="multipart/form-data">
              @csrf
              <input type="hidden" value="{{$facultyEdit->id}}" name="faculty_id">
              <div class="box-body">
                <div class="form-group">
                  <label for="name">Faculty Name</label>
                  <input type="text" class="form-control" id="name" name="name" placeholder="Enter Faculty Name" value="{{old('name',$facultyEdit->name)}}">
                </div>
                <div class="form-group">
                  <label for="subject">Faculty Subject</label>
                  <input type="text" class="form-control" id="subject" name="subject" placeholder="Enter Faculty Subject" value="{{old('subject',$facultyEdit->subject)}}">
                </div>
                <div class="form-group">
                  <label for="class">Faculty Class</label>
                  <input type="text" class="form-control" id="class" name="class" placeholder="Enter Faculty Class" value="{{old('class',$facultyEdit->class)}}">
                </div>
               </div>
              <!-- /.box-body -->
               <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
            </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
@endsection