@extends('layouts.main')
@section('dynamic')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
  <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Faculty Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>Faculty Name</th>
                  <th>Faculty Subject</th>
                  <th>Faculty Class</th>
                  <th>Time</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($facultyDetails as $facultyDetail)
                        <tr>
                            <td>{{$facultyDetail -> name}}</td>
                            <td>{{$facultyDetail -> subject}}</td>
                            <td>{{$facultyDetail -> class}}</td>
                             <td>{{date('d-M-Y',$facultyDetail -> time)}}</td>
                            <td class="btn-group">
                              <div class="btn-group">
                                <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle"> Action<span class="caret"></span></button>
                                    <ui role="menu" class="dropdown-menu">
                                       <li><a href="{{route('faculty.edit',$facultyDetail ->id)}}" class="dropdown-item" style="padding-left: 15px;"><span class="glyphicon glyphicon-edit"></span> Edit</a></li> 
                                        <li><a href="{{route('faculty.delete',$facultyDetail ->id)}}" class="dropdown-item" style="padding-left: 15px; color: red;"><span class="glyphicon glyphicon-trash"></span> Delete</a></li>
                                    </ui>
                              </div>
                             </td>
                         </tr>
                  @endforeach
                </tbody>
             </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
</section>
@endsection