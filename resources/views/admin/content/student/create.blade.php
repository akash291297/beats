@extends('layouts.main')
@section('dynamic')
<section class="content">
    <div class="row">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
        
        <!-- left column -->
    <div class="col-md-6 col-md-offset-3">
	<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Students</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{route('student.store')}}" method="post" enctype="multipart/form-data">
            	@csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="roll">Roll No.</label>
                  <input type="number" class="form-control" id="roll" name="roll" placeholder="Enter Student Roll No" value="{{old('roll')}}">
                </div> 
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" class="form-control" id="name" name="name" placeholder="Enter Student Name" value="{{old('name')}}">
                </div>
                <div class="form-group">
                    @if(count($classDetails)>0)
                        <label for="class">Class</label>
                        <select name="class" id="class" class="form-control">
                            <option value="" selected disabled>Select Class</option>
                            @foreach($classDetails as $classDetail)
                                <option value="{{$classDetail->class}}">{{$classDetail->class}}</option>
                            @endforeach  
                        </select>
                     @endif
                </div>
                <div class="form-group">
                  <label for="school">School</label>
                  <input type="text" id="school"  class="form-control" name="school"  placeholder="Enter School Name" value="{{old('school')}}">				
                </div>
				<div class="form-group">
                  <label for="image">Profile Pic</label>
                  <input type="file" id="image"  class="form-control" name="image" placeholder="Choose Profile Pic" value="{{old('image')}}">
				</div>
				<div class="form-group">
                    @if(count($batchDetails)>0)
                        <label for="batch">Batch</label>
                        @foreach($batchDetails as $batchDetail)
                            <select name="batch" id="batch" class="form-control">
                                <option value="" selected disabled>Choose Batch</option>
                                <option value="{{$batchDetail->id}}">{{$batchDetail->name}}</option>
                            </select>
                        @endforeach    
                    @endif  
                </div>
				<div class="form-group">
                  <label for="address">Address</label>
                  <input type="text" id="address"  class="form-control" name="address" placeholder="Enter Student Address" value="{{old('address')}}">
				</div>
				<div class="form-group">
                  <label for="fname">Father Name</label>
                  <input type="text" id="fname"  class="form-control" name="fname" placeholder="Enter Father Name" value="{{old('fname')}}">
				</div>
				<div class="form-group">
                  <label for="mname">Mother Name</label>
                  <input type="text" id="mname"  class="form-control" name="mname" placeholder="Enter Mother Name" value="{{old('mname')}}">
				</div>
				<div class="form-group">
                  <label for="fcontact">Father Contact</label>
                  <input type="text" id="fcontact"  class="form-control" name="fcontact" placeholder="Enter Father Contact" value="{{old('fcontact')}}">
				</div>
				<div class="form-group">
                  <label for="mcontact">Mother Contact</label>
                  <input type="text" id="mcontact"  class="form-control" name="mcontact" placeholder="Enter Mother Contact" value="{{old('mcontact')}}">
				</div>
				<div class="form-group">
                  <label for="scontact">Student Contact</label>
                  <input type="text" id="scontact"  class="form-control" name="scontact" placeholder="Enter Student Contact" value="{{old('scontact')}}">
				</div>


                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
            </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
@endsection