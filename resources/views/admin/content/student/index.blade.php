@extends('layouts.main')
@section('dynamic')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
	<div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Student Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-hover table-responsive">
                <thead>
                <tr>
                  <th>Roll No</th>
                  <th>Name</th>
                  <th>Class</th>
                  <th>School</th>
                  <th>Profile Pic</th>
                  <th>Batch</th>
                  <th>Address</th>
                  <th>Father Name</th>
                  <th>Mother Name</th>
                  <th>Father Contact</th>
                  <!-- <th>Mother Contact</th> -->
                  <!-- <th>Student Contact</th> -->
                  <th>Time</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($studentDetails as $studentDetail)
                        <tr>
                            <td>{{$studentDetail -> roll_no}}</td>
                            <td>{{$studentDetail -> name}}</td>
                            <td>{{$studentDetail -> class}}</td>
                            <td>{{$studentDetail -> school}}</td>
                            <td><img src="{{asset($studentDetail -> profilepic)}}" width="120px" height="70px"></td>
                            <td>{{$studentDetail -> batch}}</td>
                            <td>{{$studentDetail -> address}}</td>
                            <td>{{$studentDetail -> father}}</td>
                            <td>{{$studentDetail -> mother}}</td>
                            <td>{{$studentDetail ->fcontact}}</td>
                            <!-- <td>{{$studentDetail ->mcontact}}</td> -->
                            <!-- <td>{{$studentDetail ->scontact}}</td> -->
                            <td>{{date('d-M-Y',$studentDetail->time)}}</td>
                            <td class="btn-group">
                              <div class="btn-group">
                                <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle"> Action<span class="caret"></span></button>
                                    <ui role="menu" class="dropdown-menu">
                                       <li><a href="{{route('student.edit',$studentDetail ->id)}}" class="dropdown-item" style="padding-left: 15px;"><span class="glyphicon    glyphicon-edit"></span> Edit</a></li> 
                                        <li><a href="{{route('student.delete',$studentDetail ->id)}}" class="dropdown-item" style="padding-left: 15px; color: red;"><span class="glyphicon glyphicon-trash"></span> Delete</a></li>
                                    </ui>
                              </div>
                             </td>
                         </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
</section>

@endsection