@extends('layouts.main')
@section('dynamic')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
	<div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Subject Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-hover ">
                <thead>
                <tr>
                  <th>Class</th>
                  <th>Subject</th>
                  <th>Time</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($subjectLists as $subjectList)
                        <tr>
                            <td>
                              @if(!empty($subjectList->class))
                              {{$subjectList -> class -> class}}
                              @else
                              Not Found
                              @endif
                            </td>
                            <td>{{$subjectList -> subject}}</td>
                            <td>{{date('d-M-Y',$subjectList -> time)}}</td>
                            <td class="btn-group">
                              <div class="btn-group">
                                <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle"> Action<span class="caret"></span></button>
                                    <ui role="menu" class="dropdown-menu">
                                       <li><a href="{{route('subject.edit',$subjectList ->id)}}" class="dropdown-item" style="padding-left: 15px;"><span class="glyphicon glyphicon-edit"></span> Edit</a></li> 
                                        <li><a href="{{route('subject.delete',$subjectList ->id)}}" class="dropdown-item" style="padding-left: 15px; color: red;"><span class="glyphicon glyphicon-trash"></span> Delete</a></li>
                                    </ui>
                              </div>
                             </td>
                         </tr>
                  @endforeach
                </tbody>
                <tfoot>
                
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
            </div>
      </div>
</section>
@endsection