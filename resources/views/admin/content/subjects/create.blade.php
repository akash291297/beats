@extends('layouts.main')
@section('dynamic')
<section class="content"> 
    <div class="row">
        <!-- left column -->
    <div class="col-md-6 col-md-offset-3">
	<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Create Subjects</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{route('subject.store')}}" method="post" enctype="multipart/form-data">
            	@csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="class">Class</label>
                  <select name="class_id" class="form-control">
                    <option  selected disabled>Select Class</option>
                    @foreach($classLists as $classList)
                    <option value="{{$classList->id}}">{{$classList->class}}</option>
                    @endforeach
                  </select>
                </div>
                 <div class="form-group">
                  <label for="subject">Subject</label>
                  <input type="text" class="form-control" id="subject" name="subject" placeholder= "Add Subjects " value="{{old('subject')}}">
                </div>
            </div>
              <!-- /.box-body -->
               <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
            </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
@endsection