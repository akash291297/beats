@extends('layouts.main')
@section('dynamic')
<section class="content"> 
    <div class="row">
        <!-- left column -->
    <div class="col-md-6 col-md-offset-3">
  <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Subject</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{route('subject.update')}}" method="post" enctype="multipart/form-data">
              @csrf
              <input type="hidden" value="{{$subjectEdit->id}}" name="subject_id">
              <div class="box-body">
                <div class="form-group">
                  <label for="class">Class</label>
                  <div class="form-group">
                <select class="form-control" name="class_id">
                  @foreach($classLists as $classList)
                  <option value="{{$classList->id}}" @if($classList->id == $subjectEdit->class_id) selected @endif>{{$classList->class}}</option>
                 @endforeach
                </select>
              </div>
                </div>
                 <div class="form-group">
                  <label for="subject">Subject</label>
                  <input type="text" class="form-control" id="subject" name="subject" placeholder="Enter Subject Name" value="{{old('subject',$subjectEdit->subject)}}">
                </div>
            </div>
              <!-- /.box-body -->
               <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
            </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
@endsection