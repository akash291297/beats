@extends('layouts.main')
@section('dynamic')
<section class="content"> 
    <div class="row">
        <!-- left column -->
    <div class="col-md-6 col-md-offset-3">
  <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Announcements</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{route('announcement.update')}}" method="post" enctype="multipart/form-data">
              @csrf
              <input type="hidden" value="{{$announcementsEdit->id}}" name="announcements_id">
              <div class="box-body">
                <div class="form-group">
                  <label for="text">Announcement</label>
                  <input type="text" class="form-control" id="text" name="text" placeholder="Enter Announcements Name" value="{{old('text', $announcementsEdit->announcement)}}">
                </div>
                
                <div class="form-group">
                          <label for="image">Announcement Image</label>
                          <input type="file" id="image"  class="form-control" name="image" placeholder="Choose Profile Pic" value="{{old('image')}}">
                </div>
              </div>
              <!-- /.box-body -->
               <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
            </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
@endsection