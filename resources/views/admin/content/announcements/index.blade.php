@extends('layouts.main')
@section('dynamic')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
	<div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Announcements Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>Announcements</th>
                  <th>Image</th>
                  <th>Batch</th>
                  <th>Roll No</th>
                  <th>Time</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($announcementDetails as $announcementDetail)
                        <tr>
                            <td>{{$announcementDetail -> announcement}}</td>
                            <td><img src="{{asset($announcementDetail -> image)}}" width="120px" height="70px"></td>
                            <td>{{$announcementDetail -> batch}}</td>
                            <td>
                            @if(count($announcementDetail->announcements)>0)
                                <ul>
                                @foreach($announcementDetail->announcements as $announce)
                                    @if(!empty($announce->roll))
                                        <li>{{$announce->roll->roll_no}}</li>
                                    @else
                                        Not Found 
                                    @endif
                                @endforeach
                                </ul>
                            @endif
                            </td>
                            <td>{{date('d-M-Y',$announcementDetail -> time)}}</td>
                            <td class="btn-group">
                              <div class="btn-group">
                                <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle"> Action<span class="caret"></span></button>
                                    <ui role="menu" class="dropdown-menu">
                                       <li><a href="{{route('announcement.edit',$announcementDetail ->id)}}" class="dropdown-item" style="padding-left: 15px;"><span class="glyphicon    glyphicon-edit"></span> Edit</a></li> 
                                        <li><a href="{{route('announcement.delete',$announcementDetail ->id)}}" class="dropdown-item" style="padding-left: 15px; color: red;"><span class="glyphicon glyphicon-trash"></span> Delete</a></li>
                                    </ui>
                              </div>
                             </td>
                         </tr>
                  @endforeach
                </tbody>
                <tfoot>
                
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
           </div>
      </div>
</section>
@endsection