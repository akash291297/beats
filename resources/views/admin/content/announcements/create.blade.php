@extends('layouts.main')
@section('dynamic')
<section class="content"> 
    <div class="row">
        <!-- left column -->
    <div class="col-md-6 col-md-offset-3">
	<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Create Announcements</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{route('announcement.store')}}" method="post" enctype="multipart/form-data">
            	@csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="text">Announcement</label>
                  <input type="text" class="form-control" id="text" name="text" placeholder="Enter Announcements Name" value="{{old('text')}}">
                </div>
                
        		<div class="form-group">
                    <label for="image">Announcement Image</label>
                        <input type="file" id="image"  class="form-control" name="image" placeholder="Choose Profile Pic" value="{{old('image')}}">
        		</div>

                <div class="form-group">
                    @if(count($studentBatchs)>0)
                        @foreach($studentBatchs as $studentBatch)
                            <label for="batch">Select Batch</label>
                            <select name="batch" class="form-control select2" multiple data-placeholder="Choose Batches" id="select_batch"> 
                                <option value="{{$studentBatch->id}}">{{$studentBatch->name}}</option>
                            </select>
                        @endforeach  
                    @endif
                </div>

                <div class="form-group">
                    <label for="roll">Select Roll No</label>
                        <select name="rolls[]" class="form-control select2" multiple id="select_roll" data-placeholder="Select Roll No">
                            <option value=""></option>
                        </select>
                </div>
				<div>
              <!-- /.box-body -->
               <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
            </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
@endsection