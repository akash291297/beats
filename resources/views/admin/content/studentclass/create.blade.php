@extends('layouts.main')
@section('dynamic')
<section class="content">
    <div class="row">
        <!-- left column -->
    <div class="col-md-6 col-md-offset-3">
	<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Create Student Class</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{route('class.store')}}" method="post" enctype="multipart/form-data">
            	@csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="class">Class</label>
                  <input type="text" class="form-control" id="class" name="class" placeholder= "Enter Class Name" value="{{old('class')}}">
                </div>
                <div class="form-group">
                  <label for="class">Status</label>
                  <select name="status" class="form-control">
                    <option value="" disabled selected>Choose Status</option>
                    <option value="a">Active</option>
                    <option value="n">InActive</option>
                  </select>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
            </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
@endsection