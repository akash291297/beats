@extends('layouts.main')
@section('dynamic')
<section class="content">
    <div class="row">
        <!-- left column -->
    <div class="col-md-6 col-md-offset-3">
  <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Student Class</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{route('class.update')}}" method="post" enctype="multipart/form-data">
              @csrf
              <input type="hidden" value="{{$classEdit->id}}" name="sclass_id">
              <div class="box-body">
                <div class="form-group">
                  <label for="class">Class</label>
                  <input type="text" class="form-control" id="class" name="class" placeholder="Enter Student Name" value="{{old('class',$classEdit->class)}}">
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
            </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
@endsection