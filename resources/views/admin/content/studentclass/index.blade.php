@extends('layouts.main')
@section('dynamic')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
	<div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Student Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>Class</th>
                  <!-- <th>Status</th> -->
                  <th>Change Class Status</th>
                  <th>Time</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($classDetails as $classDetail)
                        <tr>
                            <td>{{$classDetail -> class}}</td>
                            <!--  <td>
                              @if($classDetail -> status == 'a')
                                 <p class="badge badge-pill badge-success">Active</p>
                                <button type="button" class="btn btn-primary rounded">Active</button>
                              @else
                                <p class="badge badge-pill badge-danger">InActive</p>
                              @endif  
                            </td> --> 
                            <td>
                              @if($classDetail -> status == 'a')
                                  <form action="{{route('class.inactive')}}" method="post">
                                    @csrf
                                    <button type="submit" class="btn btn-danger btn-sm rounded" value="{{$classDetail->id}}" name="nclass_id" data-toggle="tooltip" data-placement="bottom" title="Click to Make Class Status Active">InActive</button>
                                  </form>
                              @else
                                  <form action="{{route('class.active')}}" method="post">
                                    @csrf
                                    <button type="submit" class="btn btn-success btn-sm rounded" value="{{$classDetail->id}}" name="aclass_id" data-toggle="tooltip" data-placement="bottom" title="Click to Make Class Status InActive">Active</button>
                                  </form>
                              @endif 
                            </td>
                            <td>{{date('d-M-Y',$classDetail -> time)}}</td>
                            <td class="btn-group">
                              <div class="btn-group">
                                <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle"> Action<span class="caret"></span></button>
                                    <ui role="menu" class="dropdown-menu">
                                       <li><a href="{{route('class.edit',$classDetail ->id)}}" class="dropdown-item" style="padding-left: 15px;"><span class="glyphicon    glyphicon-edit"></span> Edit</a></li> 
                                        <li><a href="{{route('class.delete',$classDetail ->id)}}" class="dropdown-item" style="padding-left: 15px; color: red;"><span class="glyphicon glyphicon-trash"></span> Delete</a></li>
                                    </ui>
                              </div>
                             </td>
                         </tr>
                  @endforeach
                </tbody>
                <tfoot>
                
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          </div>
      </div>
</section>
@endsection