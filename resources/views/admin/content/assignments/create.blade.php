@extends('layouts.main')
@section('dynamic')
<section class="content"> 
    <div class="row">
        <!-- left column -->
    <div class="col-md-6 col-md-offset-3">
	<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Create Assignment</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{route('assignment.store')}}" method="post" enctype="multipart/form-data">
            	@csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="text">Assignment</label>
                  <input type="text" class="form-control" id="text" name="text" placeholder="Enter Assignment Name" value="{{old('text')}}">
                </div>
                 <div class="form-group">
                  <label for="topic">Topic</label>
                  <input type="text" class="form-control" id="topic" name="topic" placeholder="Enter Assignment Topic" value="{{old('topic')}}">
                </div>
                <div class="form-group">
                  <label>Submission Date and Time</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="reservationtime" name="date">
                  </div>
               </div>
                <div class="form-group">
                          <label for="image">Announcement Image</label>
                          <input type="file" id="image"  class="form-control" name="image" placeholder="Choose Profile Pic" value="{{old('image')}}">
        				</div>
				    </div>
              <!-- /.box-body -->
               <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
            </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
@endsection