@extends('layouts.main')
@section('dynamic')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Batches Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>Batch Name</th>
                  <th>Class</th>
                  <th>Subject</th>
                  <th>Center</th>
                  <th>Date</th>
                  <th>Start Time</th>
                  <th>End Time</th>
                  <th>WeekDays</th>
                  <th>Created Time</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                        @if(count($batchDetails)>0)
                        @foreach($batchDetails as $batchDetail)
                        <tr>
                            <td>{{$batchDetail -> name}}</td>
                             <td>
                                @if(!empty($batchDetail->class))
                                   {{$batchDetail -> class->class}}
                                @else
                                Not Found   
                                @endif
                            </td>
                            <td>
                                @if(!empty($batchDetail->subject))
                                    {{$batchDetail->subject->subject}}
                                @else
                                Not Found
                                @endif
                            </td>
                            <td>
                                @if($batchDetail -> center == 1)
                                    Molarband
                                @else
                                    Jaitpur
                                @endif
                            </td>
                            <td>{{date('d-M-Y',$batchDetail -> time)}}</td>
                            <td>{{date('h:i:s a',strtotime($batchDetail -> starttime))}}</td>
                            <td>{{date('h:i:s a',strtotime($batchDetail -> endtime))}}</td>
                            <td>
                              @if(count($batchDetail->batchweek)>0)
                                    <ul>
                                      @foreach($batchDetail->batchweek as $week)
                                        @if(!empty($week->weekday))
                                        <li>{{$week->weekday->weekdays}}</li>
                                        @else
                                        Not Found
                                        @endif
                                      @endforeach
                                    </ul>
                              @endif
                            </td>
                            <td>{{date('d-M-Y',$batchDetail -> time)}}</td>
                            <td class="btn-group">
                              <div class="btn-group">
                                <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle"> Action<span class="caret"></span></button>
                                    <ui role="menu" class="dropdown-menu">
                                       <li><a href="{{route('batch.edit',$batchDetail ->id)}}" class="dropdown-item" style="padding-left: 15px;"><span class="glyphicon glyphicon-edit"></span> Edit</a></li> 
                                        <li><a href="{{route('batch.delete',$batchDetail ->id)}}" class="dropdown-item" style="padding-left: 15px; color: red;"><span class="glyphicon glyphicon-trash"></span> Delete</a></li>
                                    </ui>
                              </div>
                             </td>
                         </tr>
                  @endforeach
                  @endif
                </tbody>
                <tfoot>
                
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
            </div>
      </div>
</section>
@endsection