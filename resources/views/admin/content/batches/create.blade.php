@extends('layouts.main')
@section('dynamic')
<section class="content">
    <div class="row">
        <!-- left column -->
    <div class="col-md-6 col-md-offset-3">
  <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Batches</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{route('batch.store')}}" method="post" enctype="multipart/form-data">
              @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="name">Batch Name</label>
                  <input type="text" class="form-control" id="name" name="name" placeholder="Enter Batch Name" value="{{old('name')}}">
                </div>
                <div class="form-group">
                  <label for="class">Select Class</label>
                  <select name="class_id" class="form-control" id="select_class">
                    <option  selected disabled>Select Class</option>
                    @foreach($classLists as $classList)
                    <option value="{{$classList->id}}">{{$classList->class}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label for="school">Choose Subject</label>
                  <select name="subject_id" class="form-control" id="select_subject">
                    <option  selected disabled>Choose Subject</option>
                    <option value=""></option>
                  </select>       
                </div>
                <div class="form-group">
                  <label for="school"> Center</label>
                  <select name="center" class="form-control" id="select_subject">
                    <option  selected disabled>Choose Center</option>
                    <option value="1">Molarband</option>
                    <option value="2">Jaitpur</option>
                  </select>       
                </div>
                <div class="form-group">
                   <div class="form-group">
                        <label>Date:</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control pull-right" name="date" id="datepicker">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                          <label>Start Time:</label>
                            <div class="input-group">
                            <input type="text" class="form-control timepicker" name="stime">
                                <div class="input-group-addon">
                              <i class="fa fa-clock-o"></i>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                         <div class="bootstrap-timepicker">
                        <div class="form-group">
                          <label>End Time:</label>
                            <div class="input-group">
                            <input type="text" class="form-control timepicker" name="etime">
                                <div class="input-group-addon">
                              <i class="fa fa-clock-o"></i>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                <label>Weekdays</label>
                <select class="form-control select2" multiple="multiple" data-placeholder="Select a Weekdays" name="week[]" 
                        style="width: 100%;">
                    @foreach($weekLists as $weekList)
                        <option value="{{$weekList->id}}">{{$weekList->weekdays}}</option>
                    @endforeach
                </select>
              </div>
                </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
            </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
@endsection