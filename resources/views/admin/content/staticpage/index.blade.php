@extends('layouts.main')
@section('dynamic')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Batches Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>Banner</th>
                  <th>Banner1</th>
                  <th>Banner2</th>
                  <th>Address</th>
                  <th>Courses</th>
                  <th>Time</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                        @if(count($staticList)>0)
                        @foreach($staticList as $static)
                        <tr>
                            <td><img src="{{asset($static->banner)}}" width="100" height="50"></td>
                            <td><img src="{{asset($static->banner1)}}" width="100" height="50"></td>
                            <td><img src="{{asset($static->banner2)}}" width="100" height="50"></td>
                            <td>{!! $static->address !!}</td>
                            <td><ul><li>{{$static->courses}}</li></ul></td>
                            <td>{{date('d-M-Y',$static->time)}}</td>
                            <td class="btn-group">
                              <div class="btn-group">
                                <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle"> Action<span class="caret"></span></button>
                                    <ui role="menu" class="dropdown-menu">
                                       <li><a href="{{route('static.edit',$static ->id)}}" class="dropdown-item" style="padding-left: 15px;"><span class="glyphicon glyphicon-edit"></span> Edit</a></li> 
                                        <li><a href="{{route('static.delete',$static->id)}}" class="dropdown-item" style="padding-left: 15px; color: red;"><span class="glyphicon glyphicon-trash"></span> Delete</a></li>
                                    </ui>
                              </div>
                             </td>
                         </tr>
                  @endforeach
                  @endif
                </tbody>
                <tfoot>
                
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
            </div>
      </div>
</section>
@endsection