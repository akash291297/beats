@extends('layouts.main')
@section('dynamic')
<section class="content">
    <div class="row">
        <!-- left column -->
    <div class="col-md-6 col-md-offset-3">
  <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Create Static Page</h3> 
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{route('static.store')}}" method="post" enctype="multipart/form-data">
              @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="image">Upload Banner</label>
                  <input type="file" class="form-control" id="image" name="image" placeholder="Uplaod Banner Image" value="{{old('image')}}">
                </div>
                <div class="form-group">
                  <label for="image">Upload Banner1</label>
                  <input type="file" class="form-control" id="image" name="image1" placeholder="Uplaod Banner Image" value="{{old('image1')}}">
                </div>
                <div class="form-group">
                  <label for="image">Upload Banner2</label>
                  <input type="file" class="form-control" id="image" name="image2" placeholder="Uplaod Banner Image" value="{{old('image2')}}">
                </div>
                <div class="form-group">
                  <label for="class">Address</label>
                   <div class="box-body pad">
                     <textarea id="editor1" name="editor1" rows="10" cols="80" value="{{old('editor1')}}">
                    </textarea>
                </div>
                </div>
                <div class="form-group">
                  <label for="courses">Courses</label>
                   <textarea name="courses" class="form-control" rows="10" cols="80" value="{{old('courses')}}"></textarea>     
                </div>
                </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
            </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
@endsection