@extends('studentlayouts.main')
@section('dynamiccontent')
	<div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Student Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped ">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Class</th>
                  <th>School</th>
                  <th>Profile Pic</th>
                  <th>Batch</th>
                  <th>Address</th>
                  <th>Father Name</th>
                  <th>Mother Name</th>
                  <th>Father Contact</th>
                  <th>Mother Contact</th>
                  <!-- <th>Student Contact</th> -->
                  <th>Time</th>
                  
                </tr>
                </thead>
                <tbody>
                  @foreach($myDetails as $myDetail)
                        <tr>
                            <td>{{$myDetail -> name}}</td>
                            <td>{{$myDetail -> class}}</td>
                            <td>{{$myDetail -> school}}</td>
                            <td><img src="{{asset($myDetail -> profilepic)}}" width="120" height="80"></td>
                            <td>{{$myDetail -> batch}}</td>
                            <td>{{$myDetail -> address}}</td>
                            <td>{{$myDetail -> father}}</td>
                            <td>{{$myDetail -> mother}}</td>
                            <td>{{$myDetail ->fcontact}}</td>
                            <td>{{$myDetail ->mcontact}}</td>
                            <!-- <td>{{$myDetail ->scontact}}</td> -->
                            <td>{{date('d-M-Y',$myDetail -> time)}}</td>
                           <!--  <td class="btn-group">
                              <div class="btn-group">
                                <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle"> Action<span class="caret"></span></button>
                                    <ui role="menu" class="dropdown-menu">
                                       <li><a href="#" class="dropdown-item" style="padding-left: 15px;"><span class="glyphicon    glyphicon-edit"></span> Edit</a></li> 
                                        <li><a href="#" class="dropdown-item" style="padding-left: 15px; color: red;"><span class="glyphicon glyphicon-trash"></span> Delete</a></li>
                                    </ui>
                              </div>
                             </td> -->
                         </tr>
                  @endforeach
                </tbody>
                <tfoot>
                
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
@endsection