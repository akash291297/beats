@extends('studentlayouts.main')
@section('dynamiccontent')
	<div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Assignments Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped ">
                <thead>
                <tr>
                  <th>Assignments</th>
                  <th>Topic</th>
                  <th>Submission Date</th>
                  <th>Attachements</th>
                  <th>Time</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($myAssignments as $myAssignment)
                        <tr>
                            <td>{{$myAssignment -> name}}</td>
                            <td>{{$myAssignment -> topic}}</td>
                            <td>{{$myAssignment -> subdate}}</td>
                            <td><img src="{{asset($myAssignment -> attachment)}}" width="150px" height="90px"></td>
                            <td>{{date('d-M-Y',$myAssignment -> time)}}</td>
                           
                         </tr>
                  @endforeach
                </tbody>
                <tfoot>
                
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
@endsection