@extends('studentlayouts.main')
@section('dynamiccontent')
	<div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Announcements Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped ">
                <thead>
                <tr>
                  <th>Announcements</th>
                  <th>Image</th>
                  <th>Batch</th>
                  <th>Roll No</th>
                  <th>Time</th>
                  <!-- <th>Action</th> -->
                </tr>
                </thead> 
                <tbody>
                @if(count($myAnnouncements)>0)
                    @foreach($myAnnouncements as $myAnnouncement)
                        <tr>
                            <td>{{$myAnnouncement -> announcement}}</td>
                            <td><img src="{{asset($myAnnouncement -> image)}}" width="150px" height="90px"></td>
                            <td>{{$myAnnouncement -> batch}}</td>
                            <td>{{$myAnnouncement -> roll_no}}</td>
                            <td>{{date('d-M-Y',$myAnnouncement -> time)}}</td>
                         
                         </tr>
                    @endforeach
                @endif
                </tbody>
                <tfoot>
                
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
@endsection